package com.natureinfotech.mybusapphilt.utils.camera

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.provider.Settings
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.natureinfotech.mybusapphilt.BuildConfig
import java.io.ByteArrayOutputStream
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class CameraUtils {

    companion object {
        fun refreshGallery(context: Context?, filePath: String) {
            MediaScannerConnection.scanFile(
                context, arrayOf(filePath), null
            ) { path, uri -> }
        }

        fun checkpermission(context: Context?): Boolean {
            return ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context!!,
                Manifest.permission.RECORD_AUDIO
            ) == PackageManager.PERMISSION_GRANTED
        }

        /**
         * Downsizing the bitmap to avoid OutOfMemory exceptions
         */
        fun optimizedBitmap(sampleSize: Int, filePath: String?): Bitmap? {
            // bitmap factory
            val options = BitmapFactory.Options()
            // downsizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = sampleSize
            return BitmapFactory.decodeFile(filePath, options)
        }

        /**
         * Checks whether device has camera or not. This method not necessary if
         * android:required="true" is used in manifest file
         */
        fun isDeviceSupportCamera(context: Context): Boolean {
            return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)
        }

        /**
         * Open device app settings to allow user to enable permissions
         */
        fun openSettings(context: Context) {
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            intent.data = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        fun getOutputMediaFileUri(context: Context?, file: File?): Uri? {
            return FileProvider.getUriForFile(context!!, "com.natureinfotech.raigadshree.provider", file!!)
        }

        /**
         * Creates and returns the image or video file before opening the camera
         */
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        fun getOutputMediaFile(type: Int): File? {
            // External sdcard location
            val mediaStorageDir = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Hilt_KadamTravels"
            )

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.e("TAG", "Oops! Failed create directory")
                    return null
                }
            }

            // Preparing media file naming convention
            // adds timestamp
            val timeStamp: String =
                SimpleDateFormat("ddmmyyyy_HHmmss", Locale.getDefault()).format(Date())
            val mediaFile: File
            if (type == 1) {
                mediaFile =
                    File(mediaStorageDir.getPath() + File.separator.toString() + "IMG_" + timeStamp + "." + "jpg")
            } else if (type == 2) {
                mediaFile =
                    File(mediaStorageDir.getPath() + File.separator.toString() + "VID_" + timeStamp + "." + "mp4")
            } else {
                return null
            }
            return mediaFile
        }

        fun getImageUri(inContext: Context, inImage: Bitmap): Uri? {
            val bytes = ByteArrayOutputStream()
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
            val path = MediaStore.Images.Media.insertImage(
                inContext.getContentResolver(),
                inImage,
                "Title",
                null
            )
            return Uri.parse(path)
        }
    }
}