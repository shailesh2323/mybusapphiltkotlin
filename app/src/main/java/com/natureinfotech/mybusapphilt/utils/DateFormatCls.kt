package com.natureinfotech.mybusapphilt.utils

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class DateFormatCls {

    companion object {

        var TAG : String = "DateFormatCls"

        var datePattern : String = "yyyy-MM-dd"
        var inputFormat : SimpleDateFormat = SimpleDateFormat(datePattern)

        var datePattern_2 : String = "yyyy-MM-dd HH:mm:ss"
        var inputFormat_2 : SimpleDateFormat = SimpleDateFormat(datePattern_2)

        var datePattern_3 : String = "dd-MMM-yyyy hh:mm aa"
        var inputFormat_3 : SimpleDateFormat = SimpleDateFormat(datePattern_3)

        fun getAge(year: Int,month: Int,day: Int) : Int {
            var dob : Calendar = Calendar.getInstance()
            var today : Calendar = Calendar.getInstance()

            dob.set(year,month,day)

            var age : Int = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR)

            if(today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
                age--
            }

            var ageInt : Int = age

            return ageInt
        }

        fun isDateValid(date : String) : Boolean {
            var sdf : SimpleDateFormat = SimpleDateFormat("yyyy-M-d")

            var valid : Boolean = false

            try {
                sdf.parse(date)
                sdf.isLenient = false
                valid = true
            }
            catch (exception : ParseException) {
                exception.message?.let { PrintMessage.logE(this.TAG,it) }
                valid = false
            }

            return valid
        }
    }

}