package com.natureinfotech.mybusapphilt.utils

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar

class PrintMessage {
    companion object {

        fun toastMessage(context: Context, message: String) {
            Toast.makeText(context,message,Toast.LENGTH_LONG).show()
        }

        fun showErrorMessage(context: Context,message: String,view: View) {
            Snackbar.make(view,message,Snackbar.LENGTH_LONG).show()
        }

        fun logE(tag: String,message: String) {
            Log.e(tag,message)
        }
    }
}