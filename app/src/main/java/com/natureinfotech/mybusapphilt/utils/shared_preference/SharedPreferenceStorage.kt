package com.natureinfotech.mybusapphilt.utils.shared_preference

import android.content.Context
import com.google.gson.JsonObject
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.utils.DateFormatCls
import com.natureinfotech.mybusapphilt.utils.PrintMessage
import dagger.hilt.android.qualifiers.ApplicationContext
import java.lang.Exception
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
public class SharedPreferenceStorage @Inject constructor(@ApplicationContext var context: Context) {

    public var TAG  : String = "SharedPreferenceStorage"

    public var NoInfo : String  = "";

    public var sharedPreference : String  = "Storage";

    /* Referesh Time */
    public var sharedPreferenceTime : Long  = 5 * 60 * 1000 * 1000 * 1000L;
    public var refreshTimeChooseDisease :  String  = "refreshTimeChooseDisease";
    /* Referesh Time */

    public var IS_FIRST_TIME_LAUNCH : String  = "IsFirstTimeLaunch";

    public var authToken : String  = "authToken";
    public var firebaseToken : String  = "firebaseToken";
    public var userId : String  = "userId";
    public var userName : String  = "userName";
    public var userMobile : String  = "userMobile";
    public var userEmail : String  = "userEmail";
    public var userRoleId : String  = "userRoleId";
    public var userUniqueCode : String  = "userUniqueCode";
    public var userDOB : String  = "userDOB";
    public var userAge : String  = "userAge";
    public var userGender : String  = "userGender";
    public var userRouteId : String  = "userRouteId";
    public var userRouteName : String  = "userRouteName";
    public var userVillageId : String  = "userVillageId";
    public var userVillageName : String  = "userVillageName";
    public var userAreaId : String  = "userAreaId";
    public var userAreaName : String  = "userAreaName";
    public var userProfilePic : String  = "userProfilePic";

    public var returnJourneyMessage : String  = "returnJourneyMessage";

    public var selectUserBoarding : String  = "selectUserBoarding";
    public var selectUserDropping : String  = "selectUserDropping";
    public var selectPassengerInfo : String  = "selectPassengerInfo";
    public var selectContactEmail : String  = "selectContactEmail";
    public var selectContactMobile : String  = "selectContactMobile";
    public var selectTotalDiscountAmt : String  = "selectTotalDiscountAmt";
    public var selectTotalOriginalAmt : String  = "selectTotalOriginalAmt";
    public var selectFinalAmt : String  = "selectFinalAmt";
    public var selectBusName : String  = "selectBusName";
    public var selectBusId : String  = "selectBusId";
    public var selectBusArrivalTime : String  = "selectBusArrivalTime";
    public var selectBusDepartTime : String  = "selectBusDepartTime";
    public var selectBusTravelingDate : String  = "selectBusTravelingDate";
    public var selectBusTravelingEndDate : String  = "selectBusTravelingEndDate";
    public var selectBusFromCity : String  = "selectBusFromCity";
    public var selectBusToCity : String  = "selectBusToCity";
    public var selectReceiptNumber : String  = "selectReceiptNumber";
    public var selectCouponId : String  = "selectCouponId";
    public var selectCouponAmount : String  = "selectCouponAmount";
    public var selectIsCouponApplied : String  = "selectIsCouponApplied";
    public var selectIsPercentage : String  = "selectIsPercentage";
    public var selectIsReturn : String  = "selectIsReturn";
    public var selectIsReturnCouponCode : String  = "selectIsReturnCouponCode";
    public var selectIsReturnCouponCodeAmt : String  = "selectIsReturnCouponCodeAmt";

    fun addInformation(key: String, value: String?) {
        try{
            context.getSharedPreferences(sharedPreference,Context.MODE_PRIVATE).edit().putString(key,value).commit()
        }
        catch (exception : Exception) {
            exception.message?.let { PrintMessage.logE(TAG, it) }
        }
    }

    fun removeInformation(key: String) {
        try {
            context.getSharedPreferences(sharedPreference,Context.MODE_PRIVATE).edit().remove(key).commit()
        }
        catch (exception: Exception) {
            exception.message?.let { PrintMessage.logE(TAG,it) }
        }
    }

    fun getInformation(key: String) : String? {

        var value : String? = ""

        try {
            value = context.getSharedPreferences(sharedPreference,Context.MODE_PRIVATE).getString(key,NoInfo)
        }
        catch (exception: Exception) {
            exception.message?.let { PrintMessage.logE(TAG,it) }
        }

        return value
    }

    fun clearStorage() {
        /* This Line to Clear all Storage */
        context.getSharedPreferences(sharedPreference,Context.MODE_PRIVATE).edit().clear().commit()
    }

    fun saveProfile(jsonObject: JsonObject) {
        try {
            var userId : String = jsonObject.get("user_id").asString
            var fullName : String = jsonObject.get("full_name").asString
            var mobileNumber : String = jsonObject.get("mobile_number").asString
            var emailId : String = jsonObject.get("email_id").asString
            var DOB : String = jsonObject.get("date_of_birth").asString
            var gender : String = jsonObject.get("gender").asString
            var roleId : String = jsonObject.get("role_id").asString
            var unicode : String = jsonObject.get("unicode").asString
            var routeId : String = jsonObject.get("route_id").asString
            var routeName : String = jsonObject.get("route_name").asString
            var villageId : String = jsonObject.get("village_id").asString
            var villageName : String = jsonObject.get("village_name").asString
            var areaId : String = jsonObject.get("area_id").asString;
            var areaName : String = jsonObject.get("area_name").asString
            var returnJourneyMessage : String = jsonObject.get("return_message").asString
            var profilePic : String = jsonObject.get("profile_pic").asString

            try {
                if(DateFormatCls.isDateValid(DOB) && !DOB.equals("0000-00-00")) {
                    var date : Date = DateFormatCls.inputFormat.parse(DOB)
                    var calendar : Calendar = Calendar.getInstance()
                    calendar.time = date

                    var day : Int = calendar.get(Calendar.DAY_OF_MONTH)
                    var month : Int = calendar.get(Calendar.MONTH)
                    var year : Int = calendar.get(Calendar.YEAR)

                    var age : Int = DateFormatCls.getAge(year,month,day)

                    this.addInformation(this.userAge,age.toString())
                }
            }
            catch(exception : Exception) {
                exception.message?.let { PrintMessage.logE(TAG,it) }
            }

            if(gender.equals("1")) {
                this.addInformation(this.userGender,context.getString(R.string.male))
            }
            else if(gender.equals("2")) {
                this.addInformation(this.userGender,context.getString(R.string.female))
            }

            this.addInformation(this.userId,userId)
            this.addInformation(this.userName,fullName)
            this.addInformation(this.userMobile,mobileNumber)
            this.addInformation(this.userEmail,emailId)
            this.addInformation(this.userDOB,DOB)
            this.addInformation(this.userRoleId,roleId)
            this.addInformation(this.userUniqueCode,unicode)

            this.addInformation(this.userRouteId,routeId)
            this.addInformation(this.userRouteName,routeName)
            this.addInformation(this.userVillageId,villageId);
            this.addInformation(this.userVillageName,villageName);
            this.addInformation(this.userAreaId,areaId);
            this.addInformation(this.userAreaName,areaName);
            this.addInformation(this.returnJourneyMessage,returnJourneyMessage);
            this.addInformation(this.userProfilePic,profilePic);
        }
        catch (exception : Exception) {
            exception.message?.let { PrintMessage.logE(TAG,it) }
        }
    }

    fun clearBusSelectData() {
        context.getSharedPreferences(sharedPreference,Context.MODE_PRIVATE).edit().remove(selectUserBoarding).commit()
        context.getSharedPreferences(sharedPreference,Context.MODE_PRIVATE).edit().remove(selectUserDropping).commit()
        context.getSharedPreferences(sharedPreference,Context.MODE_PRIVATE).edit().remove(selectPassengerInfo).commit()
        context.getSharedPreferences(sharedPreference,Context.MODE_PRIVATE).edit().remove(selectContactEmail).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectContactMobile).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectTotalDiscountAmt).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectTotalOriginalAmt).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectFinalAmt).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectBusName).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectBusId).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectBusArrivalTime).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectBusDepartTime).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectBusTravelingDate).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectBusTravelingEndDate).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectBusFromCity).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectBusToCity).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectReceiptNumber).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectCouponId).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectCouponAmount).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectIsCouponApplied).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectIsPercentage).commit()
        context.getSharedPreferences(sharedPreference, Context.MODE_PRIVATE).edit().remove(selectIsReturn).commit()
    }
}