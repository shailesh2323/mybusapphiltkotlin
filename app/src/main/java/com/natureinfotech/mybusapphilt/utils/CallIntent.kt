package com.natureinfotech.mybusapphilt.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.natureinfotech.mybusapphilt.ui.dashboard.Activity_Dashboard

class CallIntent {

    companion object {
        fun callDashboard(context : Context,flag : String) {
            val intent : Intent = Intent(context,Activity_Dashboard::class.java)
            intent.putExtra(Constant.flag,flag)
            context.startActivity(intent)
            (context as Activity).finish()
        }
    }

}