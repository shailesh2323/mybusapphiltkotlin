package com.natureinfotech.mybusapphilt.utils.progress_dialog

import android.app.Activity
import android.app.Dialog
import android.util.Log
import android.view.Window
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.natureinfotech.mybusapphilt.R


class ProgressDialogCustom(var activity: Activity) {
    var dialog: Dialog? = null
    var loadImageGif = 0
    private var cancelable = true

    //set drawable of loading gif
    fun setLoadImage(img: Int) {
        loadImageGif = img
    }

    //(optional)if user can cancel(click out of layout)
    fun setCancelable(state: Boolean) {
        cancelable = state
    }

    fun show() {
        if (loadImageGif != 0) {
            dialog = Dialog(activity, R.style.mydialog)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)

            //inflate the layout
            dialog!!.setContentView(R.layout.progress_bar)

            //setup cancelable, default=false
            dialog!!.setCancelable(cancelable)

            // Set Background Transparent
            dialog!!.getWindow()!!.setBackgroundDrawableResource(R.color.color_transparent)

            //get imageview to use in Glide
            val imageView: ImageView = dialog!!.findViewById(R.id.custom_loading_imageView)

            //load gif and callback to imageview
            Glide.with(activity)
                .load(loadImageGif)
                .placeholder(loadImageGif)
                .error(loadImageGif)
                .centerCrop()
                .into(imageView)
            dialog!!.show()

        } else {
            Log.e(
                "LoadingDialog",
                "Error, missing drawable of imageloading (gif), please, use setLoadImage(R.drawable.name)."
            )
        }
    }

    //Dismiss the dialog
    fun dismiss() {
        dialog!!.dismiss()
    }

}