package com.natureinfotech.mybusapphilt.utils

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}