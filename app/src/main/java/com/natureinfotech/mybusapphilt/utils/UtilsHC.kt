package com.natureinfotech.mybusapphilt.utils


import android.content.Context
import android.content.Intent
import com.natureinfotech.mybusapphilt.R


class UtilsHC {

    companion object {

        fun shareAPKLink(
            context: Context,
            subject: String?,
            message: String?
        ) {
            try
            {
                /*Create an ACTION_SEND Intent*/
                val intent = Intent(Intent.ACTION_SEND)

                /*The type of the content is text, obviously.*/
                intent.type = "text/plain"

                /*Applying information Subject and Body.*/
                intent.putExtra(Intent.EXTRA_SUBJECT, subject)
                intent.putExtra(Intent.EXTRA_TEXT, message)

                /*Fire!*/
                context.startActivity(Intent.createChooser(intent, context.getString(R.string.app_name)))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}