package com.natureinfotech.mybusapphilt.utils

import android.util.Patterns

class FieldsValidation {

    companion object {

        fun isEmailValid(emailAddress : String) : Boolean {
            return Patterns.EMAIL_ADDRESS.matcher(emailAddress).matches()
        }

        fun isPhoneNumberValid(phoneNumber : String) : Boolean {
            return phoneNumber.length == 10
        }

    }

}