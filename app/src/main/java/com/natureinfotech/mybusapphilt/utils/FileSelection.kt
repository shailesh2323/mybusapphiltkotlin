package com.natureinfotech.mybusapphilt.utils


import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.rotateImage
import com.bumptech.glide.request.RequestOptions
import java.io.File
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import androidx.annotation.RequiresApi;
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.utils.camera.CameraUtils


class FileSelection {

    companion object {
        fun selectImageFromGallery(activity: Activity, RESULT_LOAD_IMAGE_Gallery: Int?) {
            try {
                val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                activity.startActivityForResult(i, RESULT_LOAD_IMAGE_Gallery!!)
            } catch (exception: Exception) {
                exception.printStackTrace()
            }
        }

        fun showPermissionsAlert(context: Context, message: String?) {
            val builder: AlertDialog.Builder = AlertDialog.Builder(context)
            builder
                .setTitle(context.getString(R.string.permission_required))
                .setMessage(message)
                .setPositiveButton(context.getString(R.string.gotosettings), DialogInterface.OnClickListener { dialog, which -> CameraUtils.openSettings(context) })
                .setNegativeButton(context.getString(R.string.cancel), DialogInterface.OnClickListener { dialog, which -> dialog.dismiss() }).show()
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        fun selectVideo(activity: Activity, RESULT_LOAD_VIDEO_Gallery: Int?) {
            try {
                val intent: Intent
                intent = if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                    Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI)
                } else {
                    Intent(Intent.ACTION_PICK, MediaStore.Video.Media.INTERNAL_CONTENT_URI)
                }
                intent.type = "video/*"
                intent.action = Intent.ACTION_GET_CONTENT
                intent.putExtra("return-data", true)
                activity.startActivityForResult(intent, RESULT_LOAD_VIDEO_Gallery!!)
            } catch (exception: Exception) {
                exception.printStackTrace()
            }
        }

        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        fun selecteDocument(activity: Activity, RESULT_LOAD_DOCUMENT: Int?) {
            try {
                val mimeTypes = arrayOf(
                    "application/msword",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document",  // .doc & .docx
                    "application/vnd.ms-powerpoint",
                    "application/vnd.openxmlformats-officedocument.presentationml.presentation",  // .ppt & .pptx
                    "application/vnd.ms-excel",
                    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",  // .xls & .xlsx
                    "text/plain",
                    "application/pdf"
                )
                val intent = Intent(Intent.ACTION_GET_CONTENT)
                intent.addCategory(Intent.CATEGORY_OPENABLE)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    intent.type = if (mimeTypes.size == 1) mimeTypes[0] else "*/*"
                    if (mimeTypes.size > 0) {
                        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
                    }
                } else {
                    var mimeTypesStr = ""
                    for (mimeType in mimeTypes) {
                        mimeTypesStr += "$mimeType|"
                    }
                    intent.type = mimeTypesStr.substring(0, mimeTypesStr.length - 1)
                }
                intent.action = Intent.ACTION_GET_CONTENT
                intent.putExtra("return-data", true)
                activity.startActivityForResult(intent, RESULT_LOAD_DOCUMENT!!)
            } catch (exception: Exception) {
                exception.printStackTrace()
            }
        }

        // Converting Bitmap image to Base64.encode String type
        fun getStringImage(bmp: Bitmap): String {
            val baos = ByteArrayOutputStream()
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val imageBytes: ByteArray = baos.toByteArray()
            return Base64.encodeToString(imageBytes, Base64.DEFAULT)
        }

        // Converting File to Base64.encode String type using Method
        fun getBase64tringFile(context: Context?, f: File): String {
            var imageEncoded = String()
            try {
                val compressedImg = compressImage(context, f.getPath())
                val myBitmap = BitmapFactory.decodeFile(compressedImg)
                val nh = (myBitmap.height * (512.0 / myBitmap.width)).toInt()
                val scaled = Bitmap.createScaledBitmap(myBitmap, 512, nh, true)
                val bitmap = fixedOrientation(scaled, f.getAbsolutePath())
                val baos = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos)
                val b: ByteArray = baos.toByteArray()
                imageEncoded = Base64.encodeToString(b, Base64.DEFAULT)
                Log.d("imageEncoded", imageEncoded)
                imageEncoded = resizeBase64Image(imageEncoded)
                Log.d("convertedEncoded", imageEncoded)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return imageEncoded
        }

        fun stringToBitmapImage(stringImage: String?): Bitmap? {
            var bitmap: Bitmap? = null
            try {
                val decodedString: ByteArray = Base64.decode(stringImage, Base64.DEFAULT)
                bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            } catch (exception: Exception) {
                exception.printStackTrace()
            }
            return bitmap
        }

        fun setImagViewAsBitmapAndCircular(
            context: Context?,
            profilePic: String?,
            imageView: ImageView?
        ) {
            try {
                Glide.with(context!!)
                    .load(stringToBitmapImage(profilePic))
                    .apply(RequestOptions().circleCrop())
                    .into(imageView!!)
            } catch (exception: Exception) {
                exception.printStackTrace()
            }
        }

        /* Create the File */
        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
        fun createFile(fileName: String): File? {
            // External sdcard location
            val mediaStorageDir = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "Hilt_KadamTravels"
            )

            // Create the storage directory if it does not exist
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.e("TAG", "Oops! Failed create directory")
                    return null
                }
            }

            // Preparing media file naming convention
            // adds timestamp
            val timeStamp: String =
                SimpleDateFormat("ddmmyyyy_HHmmss", Locale.getDefault()).format(Date())
            return File(mediaStorageDir.getPath() + File.separator.toString() + fileName + "_" + timeStamp + "." + "pdf")
        }

        fun compressImage(context: Context?, path: String): String {
            var scaledBitmap: Bitmap? = null
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            var bmp = BitmapFactory.decodeFile(path, options)
            var actualHeight = options.outHeight
            var actualWidth = options.outWidth

//      max Height and width values of the compressed image is taken as 816x612
            val maxHeight = 816.0f
            val maxWidth = 612.0f
            var imgRatio = actualWidth / actualHeight.toFloat()
            val maxRatio = maxWidth / maxHeight

//      width and height values are set maintaining the aspect ratio of the image
            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight
                    actualWidth = (imgRatio * actualWidth).toInt()
                    actualHeight = maxHeight.toInt()
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth
                    actualHeight = (imgRatio * actualHeight).toInt()
                    actualWidth = maxWidth.toInt()
                } else {
                    actualHeight = maxHeight.toInt()
                    actualWidth = maxWidth.toInt()
                }
            }

//      setting inSampleSize value allows to load a scaled down version of the original image
            options.inSampleSize =
                calculateInSampleSize(options, actualWidth, actualHeight)

//      inJustDecodeBounds set to false to load the actual bitmap
            options.inJustDecodeBounds = false

//      this options allow android to claim the bitmap memory if it runs low on memory
            options.inPurgeable = true
            options.inInputShareable = true
            options.inTempStorage = ByteArray(16 * 1024)
            try {
//          load the bitmap from its path
                bmp = BitmapFactory.decodeFile(path, options)
            } catch (exception: OutOfMemoryError) {
                exception.printStackTrace()
            }
            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888)
            } catch (exception: OutOfMemoryError) {
                exception.printStackTrace()
            }
            val ratioX = actualWidth / options.outWidth.toFloat()
            val ratioY = actualHeight / options.outHeight.toFloat()
            val middleX = actualWidth / 2.0f
            val middleY = actualHeight / 2.0f
            val scaleMatrix = Matrix()
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)
            val canvas = scaledBitmap?.let { Canvas(it) }
            canvas?.setMatrix(scaleMatrix)
            canvas?.drawBitmap(
                bmp,
                middleX - bmp.width / 2,
                middleY - bmp.height / 2,
                Paint(Paint.FILTER_BITMAP_FLAG)
            )

//      check the rotation of the image and display it properly
            val exif: ExifInterface
            try {
                exif = ExifInterface(path)
                val orientation: Int = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0
                )
                Log.d("EXIF", "Exif: $orientation")
                val matrix = Matrix()
                if (orientation == 6) {
                    matrix.postRotate(90F)
                    Log.d("EXIF", "Exif: $orientation")
                } else if (orientation == 3) {
                    matrix.postRotate(180F)
                    Log.d("EXIF", "Exif: $orientation")
                } else if (orientation == 8) {
                    matrix.postRotate(270F)
                    Log.d("EXIF", "Exif: $orientation")
                }
                scaledBitmap = Bitmap.createBitmap(
                    scaledBitmap!!, 0, 0,
                    scaledBitmap.width, scaledBitmap.height, matrix,
                    true
                )
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return path
        }

        fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
            // Raw height and width of image
            val height = options.outHeight
            val width = options.outWidth
            var inSampleSize = 1
            if (height > reqHeight || width > reqWidth) {
                // Calculate the largest inSampleSize value that is a power of 2 and keeps both
                // height and width lower or equal to the requested height and width.
                while (height / inSampleSize > reqHeight || width / inSampleSize > reqWidth) {
                    inSampleSize *= 2
                }
            }
            return inSampleSize
        }

        fun resizeBase64Image(base64image: String): String {
            val encodeByte: ByteArray = Base64.decode(base64image.toByteArray(), Base64.DEFAULT)
            val options = BitmapFactory.Options()
            options.inPurgeable = true
            var image = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size, options)
            if (image.height <= 400 && image.width <= 400) {
                return base64image
            }
            image = Bitmap.createScaledBitmap(image, 512, 512, false)
            val baos = ByteArrayOutputStream()
            image.compress(Bitmap.CompressFormat.PNG, 100, baos)
            val b: ByteArray = baos.toByteArray()
            System.gc()
            return Base64.encodeToString(b, Base64.NO_WRAP)
        }

        fun fixedOrientation(myBitmap: Bitmap, path: String?): Bitmap {
            var myBitmap = myBitmap
            try {
                val ei = ExifInterface(path!!)
                val orientation: Int = ei.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED
                )
                myBitmap = when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(myBitmap, 90)
                    ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(myBitmap, 180)
                    ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(myBitmap, 270)
                    ExifInterface.ORIENTATION_NORMAL -> myBitmap
                    else -> myBitmap
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return myBitmap
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    fun captureImage(activity: Activity, CAMERA_CAPTURE_IMAGE_REQUEST_CODE: Int?): String {
        var fileStoragePath = String()
        try {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val file: File? = CameraUtils.getOutputMediaFile(1)
            if (file != null) {
                fileStoragePath = file.getAbsolutePath()
            }
            val fileUri: Uri? = CameraUtils.getOutputMediaFileUri(activity.applicationContext, file)
            intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
            intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION
            // start the image capture Intent
            activity.startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE!!)
        } catch (exception: Exception) {
            exception.printStackTrace()
        }
        return fileStoragePath
    }


}