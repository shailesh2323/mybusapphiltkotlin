package com.natureinfotech.mybusapphilt.di.module

import android.text.TextUtils
import com.natureinfotech.mybusapphilt.BuildConfig
import com.natureinfotech.mybusapphilt.data.api.ApiHelper
import com.natureinfotech.mybusapphilt.data.api.ApiHelperImpl
import com.natureinfotech.mybusapphilt.data.api.ApiService
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class ApplicationModule {

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class HttpLogInterceptor

    @Qualifier
    @Retention(AnnotationRetention.BINARY)
    annotation class AuthInterceptor

    @Provides
    fun provideBaseUrl() = BuildConfig.BASE_URL;

    @HttpLogInterceptor
    @Provides
    fun provideHttpLoggingInterceptor() : HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @AuthInterceptor
    @Provides
    fun provideAuthInterceptor(sharedPreferenceStorage: SharedPreferenceStorage) : Interceptor {
        var request : Interceptor = object : Interceptor {
            override fun intercept(chain: Interceptor.Chain): Response {

                var request : Request = chain.request()

                var requestBuilder : Request.Builder = request.newBuilder()
                    .addHeader("Accept","application/json")
                    .addHeader("Content-Type", "application/json")

                if(!TextUtils.isEmpty(sharedPreferenceStorage.getInformation(sharedPreferenceStorage.authToken))) {
                    requestBuilder.addHeader("Authorization","Bearer "+sharedPreferenceStorage.getInformation(sharedPreferenceStorage.authToken))
                }

                var request2 : Request = requestBuilder.build()

                return chain.proceed(request2)
            }
        }
        return request
    }


    @Provides
    fun provideOkHttpClient(@HttpLogInterceptor httpLoggingInterceptor: HttpLoggingInterceptor,
                            @AuthInterceptor authInterceptor: Interceptor) = if(BuildConfig.DEBUG) {
        val okHttpClient = OkHttpClient
            .Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60,TimeUnit.SECONDS)
            .writeTimeout(60,TimeUnit.SECONDS)

        okHttpClient.addInterceptor(httpLoggingInterceptor)
        okHttpClient.addInterceptor(authInterceptor)

        okHttpClient.build()
    } else {
        val okHttpClient = OkHttpClient
                .Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60,TimeUnit.SECONDS)
                .writeTimeout(60,TimeUnit.SECONDS)

        okHttpClient.addInterceptor(httpLoggingInterceptor)
        okHttpClient.addInterceptor(authInterceptor)

        okHttpClient.build()
    }

    @Provides
    @Singleton
    fun provideRetrofit(
        okHttpClient: OkHttpClient,
        BASE_URL: String
    ) : Retrofit = Retrofit.Builder()
                            .baseUrl(BASE_URL)
                            .client(okHttpClient)
                            .addConverterFactory(GsonConverterFactory.create())
                            .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit) : ApiService = retrofit.create(ApiService::class.java)

    @Provides
    @Singleton
    fun provideApiHelper(apiHelperImpl: ApiHelperImpl) : ApiHelper = apiHelperImpl
}