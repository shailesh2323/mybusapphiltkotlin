package com.natureinfotech.mybusapphilt.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.response.BusList
import com.natureinfotech.mybusapphilt.databinding.RowBusListBinding
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Adapter_Bus_List @Inject constructor(@ApplicationContext var context: Context) :
    RecyclerView.Adapter<Adapter_Bus_List.ViewHolder>() {

    private val callBack = object : DiffUtil.ItemCallback<BusList.Data>() {
        override fun areItemsTheSame(oldItem: BusList.Data, newItem: BusList.Data): Boolean {
            return oldItem.bus_id == newItem.bus_id
        }

        override fun areContentsTheSame(oldItem: BusList.Data, newItem: BusList.Data): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, callBack)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var inflater: LayoutInflater = LayoutInflater.from(parent.context)
        var rowBusListBinding: RowBusListBinding =
            DataBindingUtil.inflate(inflater, R.layout.row_bus_list, parent, false)
        return ViewHolder(rowBusListBinding)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var bus: BusList.Data = differ.currentList[position]

        holder.bind(bus)
    }

    inner class ViewHolder constructor(var binding: RowBusListBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(bus: BusList.Data) {
            binding.tvName.text = context.getString(R.string.bus_name) + " : " + bus.name
            //binding.tvTotTime.text = context.getString(R.string.bus_timing)+" : "+bus.start_time+" : "+bus.end_time
            binding.tvTotalSeat.text = context.getString(R.string.seat_available) + " : " + bus.total_seats

            binding.idLayMain.setOnClickListener {
                onItemClickListner?.let {
                    it(bus)
                }
            }
        }
    }

    private var onItemClickListner: ((BusList.Data) -> Unit)? = null
    fun setOnItemClickListner(listner: (BusList.Data) -> Unit) {
        onItemClickListner = listner
    }
}