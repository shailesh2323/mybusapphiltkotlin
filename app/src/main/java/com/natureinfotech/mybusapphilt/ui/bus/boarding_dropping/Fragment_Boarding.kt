package com.natureinfotech.mybusapphilt.ui.bus.boarding_dropping

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.response.BusList
import com.natureinfotech.mybusapphilt.databinding.FragmentBoardingBinding
import com.natureinfotech.mybusapphilt.ui.adapter.Adapter_Boarding
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class Fragment_Boarding : Fragment() {

    lateinit var binding: FragmentBoardingBinding
    lateinit var bus : BusList.Data

    @Inject lateinit var sharedPreferenceStorage: SharedPreferenceStorage
    @Inject lateinit var adapter:  Adapter_Boarding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_boarding, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bus = arguments?.getSerializable("bus") as BusList.Data
        setRecyclerView()
    }

    fun setRecyclerView() {
        var alredySelectedBording = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectUserBoarding)

        var arrayList : List<BusList.Data.Boardings> = bus.boardings

        if(arrayList.isNotEmpty()) {
            binding.idRecycleBording.adapter = adapter
            var layoutManager : RecyclerView.LayoutManager = LinearLayoutManager(this.context,LinearLayoutManager.VERTICAL,false)
            binding.idRecycleBording.layoutManager = layoutManager
            adapter.differ.submitList(arrayList)
            adapter.getTheInformation(alredySelectedBording!!,bus.default_price)

            adapter.setOnItemClickListner {
                var boarding = it as BusList.Data.Boardings

                sharedPreferenceStorage.addInformation(sharedPreferenceStorage.selectUserBoarding,boarding.address)

                if(bus.default_price.equals("1")) {
                    var strJson : String = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectPassengerInfo)!!
                    var jsonElement : JsonElement = JsonParser().parse(strJson)
                    var jsonArrayPassengerInfo : JsonArray = jsonElement.asJsonArray

                    for (index in 0 until jsonArrayPassengerInfo.size()) {
                        var jsonObject : JsonObject = jsonArrayPassengerInfo.get(index).asJsonObject
                        jsonObject.addProperty("original_price",boarding.original_price)
                        jsonObject.addProperty("discount_price",boarding.discount_price)
                    }
                    sharedPreferenceStorage.addInformation(sharedPreferenceStorage.selectPassengerInfo,jsonArrayPassengerInfo.toString())
                }
                (activity as Activity_Boarding_Dropping).binding.idTabLayout.getTabAt(1)?.select()
            }
        }
        else {
            binding.idRecycleBording.visibility = View.GONE
            binding.idTxtNoDataFound.visibility = View.VISIBLE
        }
    }

}