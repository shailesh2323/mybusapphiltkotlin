package com.natureinfotech.mybusapphilt.ui.bus.bus_passenger_info

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.model.PassengerInfo
import com.natureinfotech.mybusapphilt.databinding.ActivityPassengerInfoBinding
import com.natureinfotech.mybusapphilt.ui.adapter.Adapter_Passenger_Info
import com.natureinfotech.mybusapphilt.ui.base.Activity_Base
import com.natureinfotech.mybusapphilt.ui.bus.bus_final.Activity_Bus_Final
import com.natureinfotech.mybusapphilt.utils.PrintMessage
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class Activity_PassengerInfo : Activity_Base() {

    lateinit var binding: ActivityPassengerInfoBinding
    @Inject
    lateinit var sharedPreferenceStorage: SharedPreferenceStorage

    @Inject
    lateinit var adapter_passenger_info: Adapter_Passenger_Info
    lateinit var arrayListPassengerInfo: List<PassengerInfo>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_passenger_info)

        setIntiliser()
        onClickListner()
        toolbar()
    }

    fun setIntiliser() {
        var mobile = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userMobile)
        var email = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userEmail)

        binding.etPhone.setText(mobile)
        binding.etEmail.setText(email)

        var strJson =
            sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectPassengerInfo)
        var jsonElement = JsonParser().parse(strJson)

        var jsonArrayPassengerInfo: JsonArray = jsonElement.asJsonArray

        val gson = Gson()
        val arrayPassengerInfo = object : TypeToken<PassengerInfo>() {}.type

        arrayListPassengerInfo =
            gson.fromJson(jsonArrayPassengerInfo.toString(), Array<PassengerInfo>::class.java)
                .toList()

        binding.recyclerviewProducts.adapter = adapter_passenger_info
        var layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerviewProducts.layoutManager = layoutManager
        adapter_passenger_info.differ.submitList(arrayListPassengerInfo)
    }

    fun onClickListner() {
        binding.idBtnProceed.setOnClickListener(this)
        binding.idTxtCopyAll.setOnClickListener(this);
    }

    override fun onClick(view: View?) {

        if (view == binding.idBtnProceed) {
            var total_original_amount = 0.0
            var total_discount_amount = 0.0

            var itemCount = binding.recyclerviewProducts.childCount

            Log.e("TAG",itemCount.toString())

            for (index in arrayListPassengerInfo.indices) {
                var viewRecycle = binding.recyclerviewProducts.getChildAt(index)

                Log.e("TAG_2",index.toString())

                var editTextPassengerName = viewRecycle.findViewById<EditText>(R.id.et_name)
                var editTextAge = viewRecycle.findViewById<EditText>(R.id.et_age)
                var radioGroup = viewRecycle.findViewById<RadioGroup>(R.id.radioGroup)
                var radMale = viewRecycle.findViewById<RadioButton>(R.id.radiomale)
                var radFemale = viewRecycle.findViewById<RadioButton>(R.id.radiofemale)

                var name = editTextPassengerName.text.toString()
                var age = editTextAge.text.toString()
                var gender = String()

                if (radMale.isChecked) {
                    gender = "1"
                } else if (radFemale.isChecked) {
                    gender = "2"
                }

                if (name == "" || name == null || age == "" || age == null) {
                    PrintMessage.showErrorMessage(
                        this,
                        getString(R.string.please_fill_passenger_info),
                        binding.idLayRoot
                    )
                    return
                }

                arrayListPassengerInfo[index].name = name
                arrayListPassengerInfo[index].gender = gender
                arrayListPassengerInfo[index].age = age

                var discount_amount = arrayListPassengerInfo[index].discount_price
                var original_amount = arrayListPassengerInfo[index].original_price

                total_discount_amount += discount_amount
                total_original_amount += original_amount
            }

            var contactEmail = binding.etEmail.text.toString()
            var contactMobile = binding.etPhone.text.toString()

            if (contactEmail != "" && contactMobile != "") {
                var gson = Gson()
                var jsonArray = gson.toJson(arrayListPassengerInfo)

                sharedPreferenceStorage.addInformation(
                    sharedPreferenceStorage.selectTotalDiscountAmt,
                    String.format("%.2f", total_discount_amount)
                )
                sharedPreferenceStorage.addInformation(
                    sharedPreferenceStorage.selectTotalOriginalAmt,
                    String.format("%.2f", total_original_amount)
                )
                sharedPreferenceStorage.addInformation(
                    sharedPreferenceStorage.selectFinalAmt,
                    String.format("%.2f", total_discount_amount)
                )
                sharedPreferenceStorage.addInformation(
                    sharedPreferenceStorage.selectContactMobile,
                    contactMobile
                )
                sharedPreferenceStorage.addInformation(
                    sharedPreferenceStorage.selectContactEmail,
                    contactEmail
                )
                sharedPreferenceStorage.addInformation(
                    sharedPreferenceStorage.selectPassengerInfo,
                    jsonArray
                )

                var intent = Intent(this,Activity_Bus_Final::class.java)
                startActivity(intent)
            } else {
                PrintMessage.showErrorMessage(
                    this,
                    getString(R.string.please_fill_contact_details),
                    binding.idLayRoot
                )
            }
        } else if (view == binding.idTxtCopyAll) {
            var viewRecycle = binding.recyclerviewProducts.getChildAt(0)
            var editTextPassengerName = viewRecycle.findViewById<EditText>(R.id.et_name)
            var editTextAge = viewRecycle.findViewById<EditText>(R.id.et_age)
            var radioGroup = viewRecycle.findViewById<RadioGroup>(R.id.radioGroup)
            var radMale = viewRecycle.findViewById<RadioButton>(R.id.radiomale)
            var radFemale = viewRecycle.findViewById<RadioButton>(R.id.radiofemale)

            var name = editTextPassengerName.text.toString()
            var age = editTextAge.text.toString()

            if(name == "" || age == "") {
                PrintMessage.showErrorMessage(this,getString(R.string.plese_enter_the_first_passenger_info),binding.idLayRoot)
                return
            }

            var gender = String()

            if (radMale.isChecked) {
                gender = "1";
            } else if (radFemale.isChecked) {
                gender = "2";
            }

            when {
                name == "" -> {
                    PrintMessage.showErrorMessage(this,getString(R.string.please_enter_first_passenger_name),binding.idLayRoot)
                }
                age == "" -> {
                    PrintMessage.showErrorMessage(this,getString(R.string.please_enter_first_passenger_age),binding.idLayRoot)
                }
                else -> {
                    for(index in arrayListPassengerInfo.indices) {
                        var passengerInfo = arrayListPassengerInfo[index]

                        passengerInfo.name = name
                        passengerInfo.gender = gender
                        passengerInfo.age = age
                    }

                    adapter_passenger_info.notifyDataSetChanged()
                }
            }
        }
    }

    fun toolbar() {
        setSupportActionBar(binding.idToolbar.idToolbar)
        supportActionBar?.title = getString(R.string.passenger_information)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_arrow_backward_white)

        binding.idToolbar.idToolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }
}