package com.natureinfotech.mybusapphilt.ui.dashboard

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.ui.base.Activity_Base
import com.natureinfotech.mybusapphilt.ui.dashboard.booking.Fragment_Booking
import com.natureinfotech.mybusapphilt.ui.dashboard.home.Fragment_Home
import com.natureinfotech.mybusapphilt.ui.dashboard.my_account.Fragment_MyAccount
import com.natureinfotech.mybusapphilt.ui.dashboard.offer.Fragment_Offer
import com.natureinfotech.mybusapphilt.utils.Constant
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_dashboard.*

@AndroidEntryPoint
class Activity_Dashboard : Activity_Base() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        val bundle : Bundle? = intent.extras

        var flag : String? = bundle?.getString(Constant.flag)

        bottomNavigation(flag)
    }

    fun bottomNavigation(flag : String?) {
        bottom_navigation.setOnNavigationItemSelectedListener(navigationItemSelectedListener)

        if(flag.equals("1")) {
            bottom_navigation.selectedItemId = R.id.navigation_home
        }
        else if(flag.equals("2")) {
            bottom_navigation.selectedItemId = R.id.navigation_my_account
        }
        else if(flag.equals("3")) {
            bottom_navigation.selectedItemId = R.id.navigation_my_bookings
        }
        else if(flag.equals("4")) {
            bottom_navigation.selectedItemId = R.id.navigation_offer
        }
        else  {
            bottom_navigation.selectedItemId = R.id.navigation_home
        }
    }

    val navigationItemSelectedListener : BottomNavigationView.OnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener {

            var fragment : Fragment

            when(it.itemId) {

                R.id.navigation_home -> {
                    fragment = Fragment_Home()
                    setCurrentItem(fragment,false,"Fragment_Home")
                    true
                }

                R.id.navigation_my_bookings -> {
                    fragment = Fragment_Booking()
                    setCurrentItem(fragment,false,"Fragment_Booking")
                    true
                }

                R.id.navigation_offer -> {
                    fragment = Fragment_Offer()
                    setCurrentItem(fragment,false,"Fragment_Offer")
                    true
                }

                R.id.navigation_my_account -> {
                    fragment = Fragment_MyAccount()
                    setCurrentItem(fragment,false,"Fragment_MyAccount")
                    true
                }

                else -> {
                    false
                }
            }
        }

    private fun setCurrentItem(fragment : Fragment,backStack : Boolean,TAG : String) {
        val fragmentManager : FragmentManager = supportFragmentManager
        val fragmentTransaction : FragmentTransaction = fragmentManager.beginTransaction()

        if(backStack) {
            fragmentTransaction.addToBackStack(TAG)
        }

        fragmentTransaction.replace(R.id.id_fragment_container_dashboard,fragment).commitAllowingStateLoss()
    }
}