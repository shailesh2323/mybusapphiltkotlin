package com.natureinfotech.mybusapphilt.ui.adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.response.BusList
import com.natureinfotech.mybusapphilt.databinding.RowBordingBinding
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Adapter_Boarding @Inject constructor(@ApplicationContext var context : Context) : RecyclerView.Adapter<Adapter_Boarding.ViewHolder>() {

    lateinit var alreadySelect : String
    lateinit var default_price : String

    private val callBack = object : DiffUtil.ItemCallback<BusList.Data.Boardings>() {
        override fun areItemsTheSame(
            oldItem: BusList.Data.Boardings,
            newItem: BusList.Data.Boardings
        ): Boolean {
            return oldItem.boarding_id == newItem.boarding_id
        }

        override fun areContentsTheSame(
            oldItem: BusList.Data.Boardings,
            newItem: BusList.Data.Boardings
        ): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this,callBack)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Adapter_Boarding.ViewHolder {
        var inflator : LayoutInflater = LayoutInflater.from(parent.context)
        var binding : RowBordingBinding = DataBindingUtil.inflate(inflator, R.layout.row_bording,parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: Adapter_Boarding.ViewHolder, position: Int) {
        var boarding = differ.currentList[position]
        holder.bind(boarding)
    }

    inner class ViewHolder constructor(var binding: RowBordingBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(boarding : BusList.Data.Boardings) {
            binding.tvName.text = boarding.name
            binding.tvPlaceName.text = boarding.address
            binding.tvTime.text = context.getString(R.string.time) + " : " + boarding.boarding_time

            if(default_price.equals("1")) {
                binding.tvPrice.visibility = View.VISIBLE
                binding.tvOriginalPrice.visibility = View.VISIBLE
                binding.tvOriginalPrice.text = context.getString(R.string.rupee_symbol)+" "+boarding.original_price
                binding.tvOriginalPrice.paintFlags = binding.tvOriginalPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                binding.tvPrice.text = context.getString(R.string.rupee_symbol)+" "+boarding.discount_price
            }
            else {
                binding.tvPrice.visibility = View.GONE
                binding.tvOriginalPrice.visibility = View.GONE
            }

            if(alreadySelect.equals(boarding.address)) {
                binding.idRadioButton.isChecked = true
                binding.idViewSelection.setBackgroundColor(context.resources.getColor(R.color.colorPrimaryBlue))
            }

            binding.idLayMain.setOnClickListener(View.OnClickListener {
                binding.idRadioButton.isChecked = true
                binding.idViewSelection.setBackgroundColor(context.resources.getColor(R.color.colorPrimaryBlue))
                onItemClickListner?.let {
                    it(boarding)
                }
            })

            binding.idRadioButton.setOnClickListener(View.OnClickListener {
                binding.idRadioButton.isChecked = true
                binding.idViewSelection.setBackgroundColor(context.resources.getColor(R.color.colorPrimaryBlue))
                onItemClickListner?.let {
                    it(boarding)
                }
            })
        }
    }

    fun getTheInformation(alreadySelect : String, default_price : String) {
        this.alreadySelect = alreadySelect
        this.default_price = default_price
    }

    private var onItemClickListner : ((BusList.Data.Boardings) -> Unit)? = null

    fun setOnItemClickListner(listner : (BusList.Data.Boardings) -> Unit) {
        onItemClickListner = listner
    }
}