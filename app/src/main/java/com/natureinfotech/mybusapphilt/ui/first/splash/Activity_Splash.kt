package com.natureinfotech.mybusapphilt.ui.first.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View

import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.natureinfotech.mybusapphilt.BuildConfig
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.response.CheckVersion
import com.natureinfotech.mybusapphilt.ui.base.Activity_Base
import com.natureinfotech.mybusapphilt.ui.first.login.Activity_Login
import com.natureinfotech.mybusapphilt.utils.CallIntent
import com.natureinfotech.mybusapphilt.utils.PrintMessage
import com.natureinfotech.mybusapphilt.utils.Status
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_splash.*
import javax.inject.Inject

@AndroidEntryPoint
class Activity_Splash : Activity_Base() {

    @Inject lateinit var sharedPreferenceStorage: SharedPreferenceStorage

    private val viewmodelSplash: ViewModel_Splash by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash);

        setupObserver()

        Handler(Looper.getMainLooper()).postDelayed({
            var versionCode : String = BuildConfig.VERSION_NAME
            viewmodelSplash.apiCheckVersion(versionCode)
        },1000)
    }

    private fun setupObserver() {
        viewmodelSplash.checkVersion.observe(this, Observer {
            when(it.status) {
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE

                    var checkVersion : CheckVersion = it.data as CheckVersion

                    var data : CheckVersion.Data = checkVersion.data

                    var stableVersion : Boolean? = data.stable_version

                    if(stableVersion!!) {

                        var userId = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userId)
                        var authToken = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.authToken)

                        if(userId.equals("") && authToken.equals("")) {
                            val intent : Intent = Intent(this, Activity_Login::class.java)
                            startActivity(intent)
                            finish()
                        }
                        else {
                            CallIntent.callDashboard(this,"1")
                        }
                    }
                    else {
                        PrintMessage.toastMessage(this,getString(R.string.app_update))
                    }
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                    PrintMessage.showErrorMessage(this.baseContext, it.message!!,lay_root)
                }
            }
        })
    }
}