package com.natureinfotech.mybusapphilt.ui.bus.bus_details

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.google.gson.Gson
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.google.gson.reflect.TypeToken
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.response.BusList
import com.natureinfotech.mybusapphilt.databinding.ActivityBusDetailsBinding
import com.natureinfotech.mybusapphilt.ui.base.Activity_Base
import com.natureinfotech.mybusapphilt.ui.bus.boarding_dropping.Activity_Boarding_Dropping
import com.natureinfotech.mybusapphilt.utils.PrintMessage
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import dagger.hilt.android.AndroidEntryPoint
import java.lang.reflect.Type
import javax.inject.Inject


@AndroidEntryPoint
class Activity_Bus_Details : Activity_Base() {

    lateinit var binding: ActivityBusDetailsBinding

    @Inject
    lateinit var sharedPreferenceStorage: SharedPreferenceStorage

    lateinit var layout: ViewGroup

    lateinit var id_lay_root: RelativeLayout
    lateinit var id_lay_next: LinearLayout
    lateinit var id_txt_selected_seat: TextView

    lateinit var seatViewList: ArrayList<TextView>
    var seatSize: Int = 100
    var seatGaping: Int = 10

    var STATUS_AVAILABLE: Int = 1
    var STATUS_BOOKED: Int = 2
    var STATUS_RESERVED: Int = 3

    lateinit var bus: BusList.Data
    var arrayListSeatSelection: ArrayList<JsonObject> = ArrayList<JsonObject>()
    var arrayListSelectedIds: ArrayList<Int> = ArrayList<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bus_details)

        layout = findViewById(R.id.layoutSeat)
        id_lay_next = findViewById(R.id.id_lay_next)
        id_txt_selected_seat = findViewById(R.id.id_txt_selected_seat)
        id_lay_root = findViewById(R.id.id_lay_root)

        seatViewList = ArrayList()

        bus = intent.getSerializableExtra("Bus") as BusList.Data

        binding.idTxtDate.setText(getString(R.string.travelling_date) + " : " + bus.travelling_date)
        binding.idTxtBusName.setText(getString(R.string.bus_name) + " : " + bus.name)
        binding.idTxtBusFromCity.setText(getString(R.string.from_city) + " : " + bus.start_city)
        binding.idTxtBusToCity.setText(getString(R.string.to_city) + " : " + bus.end_city)

        var seatsArray: List<List<BusList.Data.Seats>> = bus.seats

        var layoutSeat = LinearLayout(this)
        var params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        layoutSeat.orientation = LinearLayout.VERTICAL
        layoutSeat.layoutParams = params
        layoutSeat.setPadding(8 * seatGaping, 8 * seatGaping, 8 * seatGaping, 8 * seatGaping)
        layout.addView(layoutSeat)

        var layout: LinearLayout? = null

        var count = 0

        var gson = Gson()
        var type: Type = object : TypeToken<BusList.Data.Seats>() {}.type

        for (seats in seatsArray) {

            var arrayList: List<BusList.Data.Seats> = seats

            layout = LinearLayout(this)
            layout.orientation = LinearLayout.HORIZONTAL
            layoutSeat.addView(layout)

            for (index in arrayList.indices) {

                var seats: BusList.Data.Seats = arrayList[index]
                var strJsonObject: String = gson.toJson(seats)

                var jsonElement: JsonElement = JsonParser().parse(strJsonObject)
                var jsonObjectSeats: JsonObject = jsonElement.asJsonObject

                var status: String = seats.status
                var number: String = seats.number

                if (status == "1" && number != "0") {
                    jsonObjectSeats.addProperty("seatStatus", STATUS_BOOKED)

                    count++

                    var view: TextView = TextView(this)
                    var layoutParams: LinearLayout.LayoutParams =
                        LinearLayout.LayoutParams(seatSize, seatSize)
                    layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                    view.layoutParams = layoutParams;
                    view.setPadding(0, 0, 0, 2 * seatGaping);
                    view.id = count;
                    view.gravity = Gravity.CENTER;
                    view.setBackgroundResource(R.drawable.icon_seats_booked);
                    view.setTextColor(Color.WHITE);
                    view.tag = jsonObjectSeats.toString();
                    view.text = number;
                    view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9F);
                    layout.addView(view);
                    seatViewList.add(view);
                    view.setOnClickListener(this);
                } else if (status == "0" && number != "0") {
                    jsonObjectSeats.addProperty("seatStatus", STATUS_AVAILABLE)

                    count++
                    val view = TextView(this)
                    val layoutParams = LinearLayout.LayoutParams(seatSize, seatSize)
                    layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping)
                    view.layoutParams = layoutParams
                    view.setPadding(0, 0, 0, 2 * seatGaping)
                    view.id = count
                    view.gravity = Gravity.CENTER
                    view.setBackgroundResource(R.drawable.icon_seats_book)
                    view.text = number
                    view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9f)
                    view.setTextColor(Color.BLACK)
                    view.tag = jsonObjectSeats.toString()
                    layout.addView(view)
                    seatViewList.add(view)
                    view.setOnClickListener(this)
                } else if (status == "R" && number != "0") {
                    jsonObjectSeats.addProperty("seatStatus", STATUS_RESERVED)

                    count++
                    var view = TextView(this)
                    var layoutParams: LinearLayout.LayoutParams =
                        LinearLayout.LayoutParams(seatSize, seatSize)
                    layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping)
                    view.layoutParams = layoutParams
                    view.setPadding(0, 0, 0, 2 * seatGaping)
                    view.layoutParams = layoutParams;
                    view.setPadding(0, 0, 0, 2 * seatGaping);
                    view.id = count
                    view.gravity = Gravity.CENTER
                    view.setBackgroundResource(R.drawable.icon_seats_reserved)
                    view.text = number
                    view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9F)
                    view.setTextColor(Color.WHITE)
                    view.tag = jsonObjectSeats.toString()
                    layout.addView(view)
                    seatViewList.add(view)
                    view.setOnClickListener(this)
                } else if (status.equals("0") && number.equals("0")) {
                    var view = TextView(this)
                    var layoutParams: LinearLayout.LayoutParams =
                        LinearLayout.LayoutParams(seatSize, seatSize)
                    layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping)
                    view.layoutParams = layoutParams
                    view.setBackgroundColor(Color.TRANSPARENT)
                    view.text = ""
                    layout.addView(view)
                }
            }
        }

        onClickListner()
        toolbar()
    }

    fun onClickListner() {
        binding.idLayNext.setOnClickListener {
            if (arrayListSeatSelection.size != 0) {
                sharedPreferenceStorage.addInformation(
                    sharedPreferenceStorage.selectPassengerInfo,
                    arrayListSeatSelection.toString()
                )
                sharedPreferenceStorage.addInformation(
                    sharedPreferenceStorage.selectBusId,
                    bus.bus_id
                )
                sharedPreferenceStorage.addInformation(
                    sharedPreferenceStorage.selectBusName,
                    bus.name
                )
                //sharedPreferenceStorage.addInformation(sharedPreferenceStorage.selectBusDepartTime,bus.start_time)
                //sharedPreferenceStorage.addInformation(sharedPreferenceStorage.selectBusArrivalTime,bus.end_time)
                sharedPreferenceStorage.addInformation(
                    sharedPreferenceStorage.selectBusTravelingDate,
                    bus.travelling_date
                )
                sharedPreferenceStorage.addInformation(
                    sharedPreferenceStorage.selectBusFromCity,
                    bus.start_city
                )
                sharedPreferenceStorage.addInformation(
                    sharedPreferenceStorage.selectBusToCity,
                    bus.end_city
                )

                val intent = Intent(this@Activity_Bus_Details, Activity_Boarding_Dropping::class.java)
                intent.putExtra("bus", bus)
                startActivity(intent)
            } else {
                PrintMessage.showErrorMessage(
                    this@Activity_Bus_Details,
                    getString(R.string.please_select_seat),
                    binding.idLayRoot
                )
            }
        }
    }

    override fun onClick(view: View?) {

        var jsonElement: JsonElement = JsonParser().parse(view?.tag.toString())
        var jsonObject: JsonObject = jsonElement.asJsonObject

        if (jsonObject.get("seatStatus").asInt == STATUS_AVAILABLE) {

            if (arrayListSelectedIds.contains(view?.id)) {
                var index: Int = arrayListSelectedIds.indexOf(view?.id)
                arrayListSeatSelection.remove(arrayListSeatSelection[index])
                arrayListSelectedIds.remove(arrayListSelectedIds[index])
                view?.setBackgroundResource(R.drawable.icon_seats_book)

                var selectedSeat = String()

                for (j in arrayListSeatSelection.indices) {
                    var jsonObject_1: JsonObject = arrayListSeatSelection[j]
                    selectedSeat += jsonObject_1.get("number").asString
                }

                id_txt_selected_seat.text = getString(R.string.selected_seat) + " : " + selectedSeat.replace(",$".toRegex(), "")
            } else {
                arrayListSelectedIds.add(view!!.id)
                arrayListSeatSelection.add(jsonObject)
                view!!.setBackgroundResource(R.drawable.icon_seats_selected)

                var selectedSeat = String()

                for (j in arrayListSeatSelection.indices) {
                    val jsonObject_1 = arrayListSeatSelection[j]
                    selectedSeat = selectedSeat + jsonObject_1["number"].asString + ","
                }

                id_txt_selected_seat.text = getString(R.string.selected_seat) + " : " + selectedSeat.replace(",$".toRegex(), "")
            }
        } else if (jsonObject.get("seatStatus").asInt == STATUS_BOOKED) {
            PrintMessage.showErrorMessage(this, "Seat " + view?.id + " is Booked", id_lay_root);
        } else if (jsonObject.get("seatStatus").asInt == STATUS_RESERVED) {
            PrintMessage.showErrorMessage(this, "Seat " + view?.id + " is Reserved", id_lay_root);
        }
    }

    fun toolbar() {
        setSupportActionBar(binding.idToolbar.idToolbar)
        supportActionBar?.title = getString(R.string.bus_details)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_arrow_backward_white)

        binding.idToolbar.idToolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }
}








































