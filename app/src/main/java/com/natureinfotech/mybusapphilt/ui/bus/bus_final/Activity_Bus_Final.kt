package com.natureinfotech.mybusapphilt.ui.bus.bus_final

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.response.Coupon
import com.natureinfotech.mybusapphilt.data.response.GenerateOrderId
import com.natureinfotech.mybusapphilt.databinding.ActivityBusFinalBinding
import com.natureinfotech.mybusapphilt.ui.base.Activity_Base
import com.natureinfotech.mybusapphilt.utils.PrintMessage
import com.natureinfotech.mybusapphilt.utils.Status
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import com.razorpay.Checkout
import com.razorpay.PaymentData
import com.razorpay.PaymentResultWithDataListener
import dagger.hilt.android.AndroidEntryPoint
import org.json.JSONObject
import java.sql.Timestamp
import javax.inject.Inject


@AndroidEntryPoint
class Activity_Bus_Final : Activity_Base(), PaymentResultWithDataListener {

    lateinit var binding: ActivityBusFinalBinding

    @Inject
    lateinit var sharedPreferenceStorage: SharedPreferenceStorage

    val viewModel: View_Model_Bus_Final by viewModels()

    lateinit var arrayListCoupons: List<Coupon.Data.Coupons>

    lateinit var checkout : Checkout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_bus_final)

        /* Remove All Coupon Relevant Data */
        sharedPreferenceStorage.removeInformation(sharedPreferenceStorage.selectIsCouponApplied)
        sharedPreferenceStorage.removeInformation(sharedPreferenceStorage.selectCouponAmount)
        sharedPreferenceStorage.removeInformation(sharedPreferenceStorage.selectCouponId)
        sharedPreferenceStorage.removeInformation(sharedPreferenceStorage.selectIsPercentage)
        sharedPreferenceStorage.removeInformation(sharedPreferenceStorage.selectIsReturn)
        sharedPreferenceStorage.removeInformation(sharedPreferenceStorage.selectIsReturnCouponCode)
        sharedPreferenceStorage.removeInformation(sharedPreferenceStorage.selectIsReturnCouponCodeAmt)

        onIntilizer()
        onClickListner()
        onSetObservables()
        toolBar()
    }

    fun onIntilizer() {
        binding.idTxtBusName.text = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectBusName)
        binding.idTxtFromCity.text = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectBusFromCity)
        binding.idTxtToCity.text = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectBusToCity)
        binding.idTxtFromTime.text = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectBusDepartTime)
        binding.idTxtToTime.text = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectBusArrivalTime)
        binding.idTxtFromDate.text = getString(R.string.travelling_date) + " : " + sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectBusTravelingDate)
        binding.idTxtOriginalPrice.text = getString(R.string.rupee_symbol) + " " + sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectTotalOriginalAmt)
        binding.idTxtDiscountPrice.text = getString(R.string.rupee_symbol) + " " + sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectTotalDiscountAmt)
        binding.idTxtFinalAmount.text = getString(R.string.rupee_symbol) + " " + sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectFinalAmt)

        val orgPrice = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectTotalOriginalAmt)!!.toFloat()
        val disPrice = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectTotalDiscountAmt)!!.toFloat()

        val totDisPrice = orgPrice - disPrice
        binding.idTxtTotalDiscount.text = getString(R.string.rupee_symbol) + " " + totDisPrice.toString()
    }

    private fun onClickListner() {
        binding.idBtnSubmit.setOnClickListener(this)
        binding.idLayCheck.setOnClickListener(this)
        binding.idTxtAvailableCoupon.setOnClickListener(this)
        binding.idTxtAvailableCoupon.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        if(view == binding.idBtnSubmit) {
            var timestamp = Timestamp(System.currentTimeMillis())
            var receiptNumber = "RS" + timestamp.time
            sharedPreferenceStorage.addInformation(sharedPreferenceStorage.selectReceiptNumber,receiptNumber)

            initilizePayment()
        }
        else if (view == binding.idLayCheck) {
            var couponCode = binding.idEditReferralCode.text.toString()
            var finalAmount = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectFinalAmt)
            var fromCity = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectBusFromCity)
            var toCity = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectBusToCity)

            val strJson = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectPassengerInfo)
            val jsonElement = JsonParser().parse(strJson)
            val seatCount = jsonElement.asJsonArray.size()

            if(couponCode != "") {
                viewModel.apiValidateCoupon(finalAmount!!,couponCode,fromCity!!,toCity!!,seatCount.toString()!!)
            }
            else {
                PrintMessage.showErrorMessage(this,getString(R.string.please_enter_coupon_code),binding.idLayRoot)
            }
        }
        else if(view == binding.idTxtAvailableCoupon) {
            viewModel.apiGetOffers()
        }
    }

    private fun toolBar() {
        setSupportActionBar(binding.idToolbar.idToolbar)
        supportActionBar!!.title = getString(R.string.booking)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.icon_arrow_backward_white)
        binding.idToolbar.idToolbar.setNavigationOnClickListener { onBackPressed() }
    }

    private fun onSetObservables() {
        // Generate Order Data
        viewModel.mutableGenerateOrderId.observe(this, Observer {
            when(it.status) {
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE

                    var generateOrder : GenerateOrderId = it.data as GenerateOrderId

                    var generateOrderData = generateOrder.data

                    startPayment(generateOrderData)
                }
                Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                }
            }
        })

        // Coupon Verify
        viewModel.mutableCouponVerify.observe(this, Observer {
            when(it.status) {
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    var jsonObject = it as JsonObject
                }
                Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                }
            }
        })

        // Sucessful Message
        viewModel.mutableMessageSuccessful.observe(this, Observer {
            when(it.status) {
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    var jsonObject = it as JsonObject
                }
                Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                }
            }
        })

        // Coupons List
        viewModel.mutableCoupon.observe(this, Observer {
            when(it.status) {
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    var jsonObject = it as JsonObject
                }
                Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                }
            }
        })
    }

    private fun initilizePayment() {
        var receiptName = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectReceiptNumber)
        var finalAmount = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectFinalAmt)

        var finalAmountFloat = finalAmount!!.toFloat()
        var finalAmountInteger = Math.round(finalAmountFloat)

        var currencyINR = "INR"
        viewModel.apiGenerateOrderId(finalAmountInteger.toString(),currencyINR,receiptName!!)
    }

    private fun startPayment(generateOrderData : GenerateOrderId.Data) {
        /**
         * Instantiate Checkout
         */
        checkout = Checkout()

        checkout.setKeyID(generateOrderData.razorpay_razorpay_key);

        /**
         * Reference to current activity
         */
        var activity = this

        /**
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try{
            val userEmail = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userEmail)
            val userMobile = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userMobile)
            val amount: String = generateOrderData.amount.toString()
            val receiptNumber: String = generateOrderData.receipt
            val orderId: String = generateOrderData.id
            val currency: String = generateOrderData.currency

            val options = JSONObject()

            options.put("name", getString(R.string.app_name))
            options.put("description", "Reference No. $receiptNumber")
            options.put("image", "https://s3.amazonaws.com/rzp-mobile/images/rzp.png")
            options.put("order_id", orderId)
            options.put("currency", currency)
            options.put("amount", amount) //pass amount in currency subunits

            val preFill = JSONObject()
            preFill.put("email", userEmail)
            preFill.put("contact", userMobile)

            options.put("prefill", preFill)

            checkout.open(activity, options)
        }
        catch (exception : Exception) {
            exception.printStackTrace()
        }
    }

    override fun onPaymentSuccess(razorpayPaymentId : String?, paymentData: PaymentData?) {
        val userId = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userId)

        val contactEmail = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectContactEmail)
        val contactMobile = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectContactMobile)
        val receiptNumber = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectReceiptNumber)
        val totalOriginalAmount = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectTotalOriginalAmt)
        val totalDiscountAmount = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectTotalDiscountAmt)
        val couponId = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectCouponId)
        val couponAmount = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectCouponAmount)
        val finalAmount = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectFinalAmt)

        val isReturned = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectIsReturn)
        val isReturnedCouponCode = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectIsReturnCouponCode)
        val isReturnedCoupounAmt = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectIsReturnCouponCodeAmt)

        val razorPayOrderId: String = paymentData!!.orderId
        val razorPayPaymentId_1: String = razorpayPaymentId.toString()
        val razorpay_signature: String = paymentData!!.signature
        val payment_status = "1"
        val payment_type = "1"

        val strJson = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectPassengerInfo)
        val jsonElement = JsonParser().parse(strJson)
        val payment_transaction = jsonElement.asJsonArray

        val hashMap: HashMap<String, Any?> = HashMap()
        hashMap["user_id"] = userId
        hashMap["contact_email_id"] = contactEmail
        hashMap["contact_number"] = contactMobile
        hashMap["receipt_number"] = receiptNumber
        hashMap["total_original_amount"] = totalOriginalAmount
        hashMap["total_discount_amount"] = totalDiscountAmount
        hashMap["coupon_id"] = couponId
        hashMap["coupon_amount"] = couponAmount
        hashMap["final_amount"] = finalAmount
        hashMap["razorpay_order_id"] = razorPayOrderId
        hashMap["razorpay_payment_id"] = razorPayPaymentId_1
        hashMap["razorpay_signature"] = razorpay_signature
        hashMap["payment_status"] = payment_status
        hashMap["payment_type"] = payment_type
        hashMap["payment_transaction"] = payment_transaction
        hashMap["is_returned"] = isReturned
        hashMap["return_coupon_code"] = isReturnedCouponCode
        hashMap["return_coupon_amount"] = isReturnedCoupounAmt

        viewModel.apiPaymentProcess(hashMap)
    }

    override fun onPaymentError(code: Int, description: String?, paymentData: PaymentData?) {
        val message = getString(R.string.payment_failed) + " " + code + " " + description
        PrintMessage.showErrorMessage(this@Activity_Bus_Final, message, binding.idLayRoot)
    }
}