package com.natureinfotech.mybusapphilt.ui.bus.bus_list

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.repository.Repository_Bus
import com.natureinfotech.mybusapphilt.data.response.BusList
import com.natureinfotech.mybusapphilt.utils.NetworkHelper
import com.natureinfotech.mybusapphilt.utils.Resource
import com.natureinfotech.mybusapphilt.utils.resource_provider.ResourceProvider
import kotlinx.coroutines.launch
import java.lang.Exception

class ViewModel_BusList @ViewModelInject constructor(
    private val repositoryBus: Repository_Bus,
    private val networkHelper: NetworkHelper,
    private val resourceProvider: ResourceProvider
) : ViewModel() {

    private val _mutableBusList = MutableLiveData<Resource<BusList>>()
    val mutableBusList : LiveData<Resource<BusList>> get() = _mutableBusList

    fun apiGetBusList(source : String,
                      destination : String,
                      date : String)
    {
        var hashMap : HashMap<String,String> = HashMap<String,String>()
        hashMap.put("start_city",source)
        hashMap.put("end_city",destination)
        hashMap.put("travelling_date",date)

        viewModelScope.launch {
            _mutableBusList.postValue(Resource.loading(null))
            try {
                if(networkHelper.isNetworkConnected()) {
                    repositoryBus.apiGetBusList(hashMap).let {
                        if(it.isSuccessful) {
                            var busList : BusList = it.body() as BusList
                            _mutableBusList.postValue(Resource.success(busList))
                        }
                        else {
                            _mutableBusList.postValue(
                                Resource.error(
                                    it.errorBody().toString(),null
                                )
                            )
                        }
                    }
                }
                else {
                    _mutableBusList.postValue(
                        Resource.error(resourceProvider.getString(R.string.no_internet_connection),null)
                    )
                }

            } catch (exception: Exception) {
                exception.printStackTrace()
                _mutableBusList.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                _mutableBusList.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }
        }
    }

}