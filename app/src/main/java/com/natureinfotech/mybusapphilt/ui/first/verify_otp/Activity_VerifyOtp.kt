package com.natureinfotech.mybusapphilt.ui.first.verify_otp

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.model.GenerateOtp
import com.natureinfotech.mybusapphilt.ui.base.Activity_Base
import com.natureinfotech.mybusapphilt.ui.dashboard.Activity_Dashboard
import com.natureinfotech.mybusapphilt.utils.CallIntent
import com.natureinfotech.mybusapphilt.utils.Constant
import com.natureinfotech.mybusapphilt.utils.PrintMessage
import com.natureinfotech.mybusapphilt.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_verify_otp.*
import java.util.concurrent.TimeUnit

@AndroidEntryPoint
class Activity_VerifyOtp : Activity_Base() , View.OnClickListener {
    private val viewModelVerifyOtp : ViewModel_VerifyOtp by viewModels()
    lateinit var generateOtp : GenerateOtp

    var countDownTimer : CountDownTimer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_otp)

        var bundle : Bundle? = intent.extras
        generateOtp = bundle!!.getSerializable(Constant.generateOtp) as GenerateOtp

        startTimer(30000);
        onClickListner()
        setObservers()
    }

    override fun onStop() {
        super.onStop()
        if(countDownTimer != null) {
            countDownTimer!!.cancel()
            countDownTimer = null
        }
    }

    private fun onClickListner() {
        btnOtpSubmit.setOnClickListener(this)
        btnResendOtp.setOnClickListener(this)
    }

    override fun onClick(view : View?) {
        when(view) {
            btnOtpSubmit -> {
                var otp : String = etOtpVerify.text.toString()
                generateOtp.roleId = "4"
                generateOtp.isSocial = "0"
                generateOtp.otp = otp
                viewModelVerifyOtp.apiVerifyOtp(generateOtp)
            }
            btnResendOtp -> {
                startTimer(30000);
                viewModelVerifyOtp.apiGenerateOtp(generateOtp)
            }
        }
    }

    fun setObservers() {
        viewModelVerifyOtp.verifyOtp.observe(this, Observer {
            when(it.status) {
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    CallIntent.callDashboard(this,"1")
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                }
            }
        })

        viewModelVerifyOtp.generateOtp.observe(this, Observer {
            when(it.status) {
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    PrintMessage.toastMessage(this,getString(R.string.otp_send_successdully))
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                }
            }
        })
    }

    fun startTimer(noOfMilliSecond : Long) {
        btnResendOtp.visibility = View.GONE
        id_txt_resend_otp_message.visibility = View.VISIBLE

        countDownTimer = object : CountDownTimer(noOfMilliSecond,1000L) {
            override fun onTick(tick : Long) {
                var hms : String = String.format("%02d : %02d",
                                                    TimeUnit.MILLISECONDS.toMinutes(tick),
                                                    TimeUnit.MILLISECONDS.toSeconds(tick) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(tick)))
                id_txt_resend_otp_message.setText(getString(R.string.resend_in)+" "+hms);
            }
            override fun onFinish() {
                id_txt_resend_otp_message.visibility = View.GONE
                btnResendOtp.visibility = View.VISIBLE
            }
        }.start()
    }
}