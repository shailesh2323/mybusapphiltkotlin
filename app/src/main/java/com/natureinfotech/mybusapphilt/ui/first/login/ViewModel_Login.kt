package com.natureinfotech.mybusapphilt.ui.first.login

import android.content.res.Resources
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.model.GenerateOtp
import com.natureinfotech.mybusapphilt.data.repository.Repository_Login
import com.natureinfotech.mybusapphilt.utils.NetworkHelper
import com.natureinfotech.mybusapphilt.utils.Resource
import kotlinx.coroutines.launch

class ViewModel_Login @ViewModelInject constructor(private val repositoryLogin : Repository_Login,
                                                   private val networkHelper: NetworkHelper) : ViewModel() {

    private val _generateOtp = MutableLiveData<Resource<GenerateOtp>>()
    val generateOtp : LiveData<Resource<GenerateOtp>> get() = _generateOtp

    public fun apiGenerateOtp(generateOtp: GenerateOtp) {
        val hashMap : HashMap<String,String> = HashMap<String,String>()
        hashMap.put("sms_key",generateOtp.smsKey);
        hashMap.put("mobile_number",generateOtp.mobileNumber);

        viewModelScope.launch {
            _generateOtp.postValue(Resource.loading(null))

            if(networkHelper.isNetworkConnected()) {
                repositoryLogin.apiGeneratedOtp(hashMap).let {
                    if(it.isSuccessful) {
                        _generateOtp.postValue(Resource.success(generateOtp));
                    }
                    else {
                        _generateOtp.postValue(Resource.error(it.errorBody().toString(),null))
                    }
                }
            }
            else {
                _generateOtp.postValue(Resource.error(Resources.getSystem().getString(R.string.no_internet_connection),null))
            }
        }

    }
}