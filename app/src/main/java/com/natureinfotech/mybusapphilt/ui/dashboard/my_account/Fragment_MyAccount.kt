package com.natureinfotech.mybusapphilt.ui.dashboard.my_account

import android.app.Dialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.api.ApiClient
import com.natureinfotech.mybusapphilt.databinding.FragmentMyAccountBinding
import com.natureinfotech.mybusapphilt.ui.first.login.Activity_Login
import com.natureinfotech.mybusapphilt.ui.webview.Activity_WebView
import com.natureinfotech.mybusapphilt.utils.FileSelection
import com.natureinfotech.mybusapphilt.utils.Status
import com.natureinfotech.mybusapphilt.utils.UtilsHC
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import javax.inject.Inject


@AndroidEntryPoint
class Fragment_MyAccount : Fragment(), View.OnClickListener {

    lateinit var binding : FragmentMyAccountBinding

    @Inject lateinit var sharedPreferenceStorage: SharedPreferenceStorage

    val viewModel: ViewModel_MyAccount by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_my_account, container, false);
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onClickListner()
        onSetObservables()
        viewModel.apiGetProfile()
    }

    fun onClickListner() {
        binding.cardProfile.setOnClickListener(this);
        binding.rlLogout.setOnClickListener(this);
        binding.rlRefer.setOnClickListener(this);
        binding.rlOffers.setOnClickListener(this);
        binding.rlHelp.setOnClickListener(this);
        binding.rlCall.setOnClickListener(this);
        binding.rlAbout.setOnClickListener(this);
        binding.rlPrivacyPolicy.setOnClickListener(this);
        binding.rlShareApp.setOnClickListener(this);
        binding.rlSmartCard.setOnClickListener(this);
    }

    override fun onClick(view: View?) {
        if(view == binding.cardProfile) {

        }
        else if(view == binding.rlLogout) {
            dialogConfirmationLogout(getString(R.string.logout),getString(R.string.are_you_sure_want_to_logout))
        }
        else if(view == binding.rlRefer) {
            val intent = Intent(activity, Activity_WebView::class.java)
            intent.putExtra("url", ApiClient.referToEarn)
            startActivity(intent)
        }
        else if(view == binding.rlRefer) {
            val intent = Intent(activity, Activity_WebView::class.java)
            intent.putExtra("url", ApiClient.referToEarn);
            startActivity(intent);
        }
        else if(view == binding.rlOffers) {
            val intent = Intent(activity, Activity_WebView::class.java)
            intent.putExtra("url", ApiClient.offers);
            startActivity(intent);
        }
        else if(view == binding.rlHelp) {
            val intent = Intent(activity, Activity_WebView::class.java)
            intent.putExtra("url", ApiClient.help);
            startActivity(intent);
        }
        else if(view == binding.rlCall) {
            val intent = Intent(activity, Activity_WebView::class.java);
            intent.putExtra("url", ApiClient.callSupport);
            startActivity(intent);
        }
        else if(view == binding.rlAbout) {
            val intent = Intent(activity, Activity_WebView::class.java)
            intent.putExtra("url", ApiClient.aboutUs);
            startActivity(intent);
        }
        else if(view == binding.rlPrivacyPolicy) {
            val intent = Intent(activity, Activity_WebView::class.java)
            intent.putExtra("url", ApiClient.privacyPolicy);
            startActivity(intent);
        }
        else if(view == binding.rlShareApp) {
            var message = getString(R.string.download_app_today_for_faster_way)+"\n"+getString(R.string.app_name_url);
            UtilsHC.shareAPKLink(requireActivity(),getString(R.string.share),message);
        }
        else if(view == binding.rlSmartCard) {
            var mobileNumber = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userMobile);

            var url = ApiClient.smartCardLink+mobileNumber;

            var browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(browserIntent);
        }
    }

    fun onSetObservables() {
        // Complete
        viewModel.mutableComplete.observe(viewLifecycleOwner, Observer {
            when(it.status) {
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                    setTheContent()
                }
                Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                }
            }
        })
    }

    fun setTheContent() {
        val userName = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userName)
        val userGender = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userGender)
        val userMobile = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userMobile)
        val userEmail = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userEmail)
        val userDOB = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userDOB)
        val userAge = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userAge)
        val profileImage = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userProfilePic)

        binding.tvName.text = if (userName == "") getString(R.string.enter_name) else userName
        binding.tvAge.text = if (userAge == "") getString(R.string.enter_dob) else getString(R.string.age) + " " + userAge
        binding.tvMobile.text = if (userMobile == "") getString(R.string.enter_mobile_number) else userMobile
        binding.tvEmail.text = if (userEmail == "") getString(R.string.enter_email_id) else userEmail

        if (profileImage != "" && profileImage != null) {
            FileSelection.setImagViewAsBitmapAndCircular(
                activity,
                profileImage,
                binding.idImgProfileLogo
            )
        }
    }

    fun dialogConfirmationLogout(title : String, message : String) {
        try {
            var dialog : Dialog = Dialog(requireActivity())
            dialog.setContentView(R.layout.dialog_confirmation)
            dialog.window?.setBackgroundDrawableResource(R.color.color_transparent)
            dialog.setCancelable(false)

            var id_txt_title = dialog.findViewById<TextView>(R.id.id_txt_title);
            var id_txt_message = dialog.findViewById<TextView>(R.id.id_txt_message);
            var id_btn_yes = dialog.findViewById<Button>(R.id.id_btn_yes);
            var id_btn_no = dialog.findViewById<Button>(R.id.id_btn_no);

            id_txt_title.text = title
            id_txt_message.text = message

            id_btn_yes.setOnClickListener {
                dialog.dismiss()
                /* Clear All Shared Preference */
                sharedPreferenceStorage.clearStorage()

                var intent = Intent(requireActivity(),Activity_Login::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
                requireActivity().finish()
            }

            id_btn_no.setOnClickListener {
                dialog.dismiss()
            }

            dialog.show()
        }
        catch (exception : Exception) {
            exception.printStackTrace()
        }
    }
}

