package com.natureinfotech.mybusapphilt.ui.webview

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.utils.progress_dialog.ProgressDialogCustom

class Fragment_WebView : Fragment() {

    lateinit var webView : WebView
    lateinit var mProgressDialog : ProgressDialogCustom

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_web_view, container, false)
        webView = view.findViewById(R.id.id_web_view)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        /* Set Progress Bar */
        mProgressDialog = ProgressDialogCustom(requireActivity())
        mProgressDialog.setLoadImage(R.raw.progress_loader_gif)

        var url : String? = arguments?.getString("url")

        var webViewSettings = webView.settings
        webViewSettings.javaScriptEnabled = true

        var webViewClient = WebViewClientImpl(requireActivity(),mProgressDialog)
        webView.webViewClient = webViewClient

        webView.loadUrl(url.toString())
    }

}