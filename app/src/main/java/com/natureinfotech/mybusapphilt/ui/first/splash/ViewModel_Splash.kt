package com.natureinfotech.mybusapphilt.ui.first.splash

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.repository.Repository_Login
import com.natureinfotech.mybusapphilt.data.response.CheckVersion
import com.natureinfotech.mybusapphilt.utils.NetworkHelper
import com.natureinfotech.mybusapphilt.utils.Resource
import com.natureinfotech.mybusapphilt.utils.resource_provider.ResourceProvider
import kotlinx.coroutines.launch
import java.lang.Exception
import kotlin.collections.HashMap

class ViewModel_Splash @ViewModelInject constructor(
    private val repositoryLogin: Repository_Login,
    private val networkHelper: NetworkHelper,
    private val resourceProvider: ResourceProvider
) : ViewModel() {

    private val _checkVersion = MutableLiveData<Resource<CheckVersion>>()
    val checkVersion: LiveData<Resource<CheckVersion>> get() = _checkVersion

    fun apiCheckVersion(buildVersion: String) {

        var hashMap: HashMap<String, String> = HashMap()
        hashMap.put("build_version", buildVersion)

        viewModelScope.launch {

            _checkVersion.postValue(Resource.loading(null))

            try {
                if (networkHelper.isNetworkConnected()) {
                    repositoryLogin.apiCheckVersion(hashMap).let {
                        if (it.isSuccessful) {
                            _checkVersion.postValue(Resource.success(it.body()))
                        } else {
                            _checkVersion.postValue(Resource.error(it.errorBody().toString(), null))
                        }
                    }
                } else {
                    _checkVersion.postValue(
                        Resource.error(
                            resourceProvider.getString(R.string.no_internet_connection), null
                        )
                    )
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
                _checkVersion.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                _checkVersion.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }

        }
    }
}