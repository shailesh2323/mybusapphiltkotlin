package com.natureinfotech.mybusapphilt.ui.bus.bus_final

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.repository.Repository_Bus
import com.natureinfotech.mybusapphilt.data.repository.Repository_Common
import com.natureinfotech.mybusapphilt.data.response.City
import com.natureinfotech.mybusapphilt.data.response.Coupon
import com.natureinfotech.mybusapphilt.data.response.GenerateOrderId
import com.natureinfotech.mybusapphilt.utils.NetworkHelper
import com.natureinfotech.mybusapphilt.utils.Resource
import com.natureinfotech.mybusapphilt.utils.resource_provider.ResourceProvider
import kotlinx.coroutines.launch
import java.lang.Exception
import java.util.*
import kotlin.collections.HashMap

class View_Model_Bus_Final @ViewModelInject constructor(
    private val repositoryBus: Repository_Bus,
    private val repositoryCommon: Repository_Common,
    private val networkHelper: NetworkHelper,
    private val resourceProvider: ResourceProvider
) : ViewModel() {

    private val _mutableGenerateOrderId = MutableLiveData<Resource<GenerateOrderId>>()
    val mutableGenerateOrderId : LiveData<Resource<GenerateOrderId>> get() = _mutableGenerateOrderId

    private val _mutableCouponVerify = MutableLiveData<Resource<JsonObject>>()
    val mutableCouponVerify : LiveData<Resource<JsonObject>> get() = _mutableCouponVerify

    private val _mutableMessageSuccessful = MutableLiveData<Resource<JsonObject>>()
    val mutableMessageSuccessful : LiveData<Resource<JsonObject>> get() = _mutableMessageSuccessful

    private val _mutableCoupon = MutableLiveData<Resource<Coupon>>()
    val mutableCoupon: LiveData<Resource<Coupon>> get() = _mutableCoupon

    fun apiGenerateOrderId(
        amount : String,
        currency: String,
        receiptNumber : String
    ) {
        var hashMap : HashMap<String,String> = HashMap<String,String>()
        hashMap.put("amount",amount)
        hashMap.put("currency",currency)
        hashMap.put("receipt",receiptNumber)

        viewModelScope.launch {
            _mutableGenerateOrderId.postValue(Resource.loading(null))
            try {
                if(networkHelper.isNetworkConnected()) {
                    repositoryBus.apiGenerateOrderId(hashMap).let {
                        if(it.isSuccessful) {
                            var generateOrderId = it.body() as GenerateOrderId
                            _mutableGenerateOrderId.postValue(Resource.success(generateOrderId))
                        }
                        else {
                            _mutableGenerateOrderId.postValue(
                                Resource.error(
                                    resourceProvider.getString(R.string.something_went_wrong), null
                                )
                            )
                        }
                    }
                }
            } catch (exception : Exception) {
                exception.printStackTrace()
                _mutableGenerateOrderId.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable : Throwable) {
                throwable.printStackTrace()
                _mutableGenerateOrderId.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }
        }
    }

    fun apiValidateCoupon(
        amount: String,
        couponCode : String,
        fromCity: String,
        toCity: String,
        seatCount : String) {
        var hashMap : HashMap<String,String> = HashMap<String,String>()
        hashMap.put("coupon_code",couponCode);
        hashMap.put("amount",amount);
        hashMap.put("total_seats",seatCount.toString());
        hashMap.put("from_city",fromCity);
        hashMap.put("seat_count",seatCount.toString());
        hashMap.put("to_city",toCity);

        viewModelScope.launch {
            _mutableCouponVerify.postValue(Resource.loading(null))
            try {
                if(networkHelper.isNetworkConnected()) {
                    repositoryBus.apiValidateCoupon(hashMap).let {
                        if(it.isSuccessful) {
                            var jsonObject = it.body()
                            Log.e("TAG",jsonObject.toString())
                        }
                        else {
                            _mutableCouponVerify.postValue(
                                Resource.error(
                                    it.errorBody().toString(),null
                                )
                            )
                        }
                    }
                }
                else {
                    _mutableCouponVerify.postValue(
                        Resource.error(resourceProvider.getString(R.string.no_internet_connection),null)
                    )
                }
            } catch (exception : Exception) {
                exception.printStackTrace()
                _mutableCouponVerify.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                _mutableCouponVerify.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }
        }
    }

    fun apiPaymentProcess(
        hashMap: HashMap<String,Any?>
    ) {
        viewModelScope.launch {
           _mutableMessageSuccessful.postValue(Resource.loading(null))

            try {
                if(networkHelper.isNetworkConnected()) {
                    repositoryBus.apiSeatBooking(hashMap).let {
                        if(it.isSuccessful) {
                            var jsonObject = it.body()
                            Log.e("TAG",jsonObject.toString())
                        }
                        else {
                            _mutableMessageSuccessful.postValue(
                                Resource.error(
                                    it.errorBody().toString(),null
                                )
                            )
                        }
                    }
                }
                else {
                    _mutableMessageSuccessful.postValue(
                        Resource.error(resourceProvider.getString(R.string.no_internet_connection),null)
                    )
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
                _mutableMessageSuccessful.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                _mutableMessageSuccessful.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }
        }
    }

    fun apiGetOffers() {
        viewModelScope.launch {

            _mutableCoupon.postValue(Resource.loading(null))

            try {
                if (networkHelper.isNetworkConnected()) {
                    repositoryCommon.apiGetOffers().let {
                        if (it.isSuccessful) {
                            var coupun: Coupon = it.body() as Coupon
                            _mutableCoupon.postValue(Resource.success(coupun))
                        } else {
                            _mutableCoupon.postValue(Resource.error(it.errorBody().toString(), null))
                        }
                    }
                } else {
                    _mutableCoupon.postValue(
                        Resource.error(
                            resourceProvider.getString(R.string.no_internet_connection), null
                        )
                    )
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
                _mutableCoupon.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                _mutableCoupon.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }
        }
    }
}