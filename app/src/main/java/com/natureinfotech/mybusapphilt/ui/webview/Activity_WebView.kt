package com.natureinfotech.mybusapphilt.ui.webview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.ui.base.Activity_Base
import com.natureinfotech.mybusapphilt.utils.progress_dialog.ProgressDialogCustom

class Activity_WebView : Activity_Base() {

    private lateinit var webView : WebView

    private lateinit var mProgressDialog : ProgressDialogCustom

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)

        webView = findViewById<WebView>(R.id.id_web_view)

        mProgressDialog = ProgressDialogCustom(this)
        mProgressDialog.setLoadImage(R.raw.progress_loader_gif)

        var url = intent.getStringExtra("url")

        var webSettings = webView.settings
        webSettings.javaScriptEnabled = true

        var webViewClient = WebViewClientImpl(this,mProgressDialog)
        webView.webViewClient = webViewClient

        webView.loadUrl(url.toString())
    }
}