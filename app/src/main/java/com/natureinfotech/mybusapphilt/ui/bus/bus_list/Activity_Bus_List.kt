package com.natureinfotech.mybusapphilt.ui.bus.bus_list

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.response.BusList
import com.natureinfotech.mybusapphilt.databinding.ActivityBusListBinding
import com.natureinfotech.mybusapphilt.ui.adapter.Adapter_Bus_List
import com.natureinfotech.mybusapphilt.ui.base.Activity_Base
import com.natureinfotech.mybusapphilt.ui.bus.bus_details.Activity_Bus_Details
import com.natureinfotech.mybusapphilt.utils.PrintMessage
import com.natureinfotech.mybusapphilt.utils.Status
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import javax.inject.Inject

@AndroidEntryPoint
class Activity_Bus_List : Activity_Base() {

    lateinit var binding : ActivityBusListBinding;

    @Inject lateinit var sharedPreferenceStorage : SharedPreferenceStorage

    val viewModel : ViewModel_BusList by viewModels()

    lateinit var arrayListBus : List<BusList.Data>
    @Inject lateinit var adapter : Adapter_Bus_List

    lateinit var source : String
    lateinit var destination : String
    lateinit var date : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_bus_list)

        try {
            sharedPreferenceStorage.removeInformation(sharedPreferenceStorage.selectUserBoarding)
            sharedPreferenceStorage.removeInformation(sharedPreferenceStorage.selectUserDropping)

            source = intent.getStringExtra("source")!!
            destination = intent.getStringExtra("destination")!!
            date = intent.getStringExtra("date")!!

            toolbar()

            onClickListner()
            onSetRecyclerView()
            onSetObservables()

            viewModel.apiGetBusList(source,destination,date)
        }
        catch (exception : Exception) {
            exception.printStackTrace()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    fun onClickListner() {
        binding.idLayMain.setOnClickListener(this)
        binding.idBtnCheck.setOnClickListener(this)
        binding.idBtnCheck2.setOnClickListener(this)
    }

    fun onSetRecyclerView() {

        arrayListBus = ArrayList<BusList.Data>()

        binding.idRecycleBus.adapter = adapter
        var layoutManager : RecyclerView.LayoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false)
        binding.idRecycleBus.layoutManager = layoutManager
        adapter.differ.submitList(arrayListBus)

        adapter.setOnItemClickListner {
            var bus = it as BusList.Data

            var intent = Intent(this,Activity_Bus_Details::class.java)
            intent.putExtra("Bus",bus)
            startActivity(intent)
        }
    }

    fun toolbar() {
        binding.idTxtDate.text = date
        setSupportActionBar(binding.idToolbar.idToolbar)
        supportActionBar?.title = "$source to $destination"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_arrow_backward_white)

        binding.idToolbar.idToolbar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    fun onSetObservables() {
        // mutableBusList
        viewModel.mutableBusList.observe(this, Observer {
            when(it.status) {
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE

                    var busList = it.data as BusList

                    arrayListBus = busList.data

                    adapter.differ.submitList(arrayListBus)
                }
                Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                }
            }
        })
    }
}