package com.natureinfotech.mybusapphilt.ui.bus.boarding_dropping

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.response.BusList
import com.natureinfotech.mybusapphilt.databinding.ActivityBoardingDroppingBinding
import com.natureinfotech.mybusapphilt.ui.base.Activity_Base
import com.natureinfotech.mybusapphilt.utils.PrintMessage
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class Activity_Boarding_Dropping : Activity_Base() {

    lateinit var binding : ActivityBoardingDroppingBinding

    @Inject
    lateinit var sharedPreferenceStorage: SharedPreferenceStorage

    lateinit var bus : BusList.Data

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_boarding_dropping)

        binding.idTabLayout.addTab(binding.idTabLayout.newTab().setText(getText(R.string.boarding_point)))
        binding.idTabLayout.addTab(binding.idTabLayout.newTab().setText(getText(R.string.dropping_point)))

        bus = intent.getSerializableExtra("bus") as BusList.Data

        toolBar()

        // Remove Values While Initilizing
        sharedPreferenceStorage.removeInformation(sharedPreferenceStorage.selectUserBoarding);
        sharedPreferenceStorage.removeInformation(sharedPreferenceStorage.selectUserDropping);

        var bundle : Bundle = Bundle()
        bundle.putSerializable("bus",bus)

        var fragmentBoarding = Fragment_Boarding()
        fragmentBoarding.arguments = bundle

        var fragmentDropping = Fragment_Dropping()
        fragmentDropping.arguments = bundle

        FragmentReplacement(fragmentBoarding,false,"Fragment_Boarding")

        binding.idTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                when(tab?.position) {
                    0 -> {
                        FragmentReplacement(fragmentBoarding,false,"Fragment_Boarding");
                    }
                    1 -> {
                        FragmentReplacement(fragmentDropping,false,"Fragment_Dropping");
                    }
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                PrintMessage.logE("Activity_Boarding_Dropping","onTabUnselected")
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                PrintMessage.logE("Activity_Boarding_Dropping","onTabReselected")
            }
        })
    }

    fun FragmentReplacement(fragment : Fragment, addToBackStack : Boolean, tag : String) {
        var fragmentManager = supportFragmentManager
        var fragmentTransaction = fragmentManager.beginTransaction()

        if(addToBackStack) {
            fragmentTransaction.addToBackStack(tag)
        }

        fragmentTransaction.replace(R.id.id_fragment_container_bord_drop,fragment).commitAllowingStateLoss()
    }

    private fun toolBar() {
        setSupportActionBar(binding.idToolbar.idToolbar)
        supportActionBar?.setTitle(R.string.boarding_and_droping)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.icon_arrow_backward_white)
        binding.idToolbar.idToolbar.setNavigationOnClickListener{
            onBackPressed()
        }
    }
}