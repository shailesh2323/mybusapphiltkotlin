package com.natureinfotech.mybusapphilt.ui.dashboard.home

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.repository.Repository_Common
import com.natureinfotech.mybusapphilt.data.response.City
import com.natureinfotech.mybusapphilt.data.response.Coupon
import com.natureinfotech.mybusapphilt.utils.NetworkHelper
import com.natureinfotech.mybusapphilt.utils.Resource
import com.natureinfotech.mybusapphilt.utils.resource_provider.ResourceProvider
import kotlinx.coroutines.launch
import java.lang.Exception

class ViewModel_Home @ViewModelInject constructor(
    private val repositoryCommon: Repository_Common,
    private val networkHelper: NetworkHelper,
    private val resourceProvider: ResourceProvider
) : ViewModel() {

    private val _mutableCity = MutableLiveData<Resource<City>>()
    val mutableCity: LiveData<Resource<City>> get() = _mutableCity

    private val _mutableCoupon = MutableLiveData<Resource<Coupon>>()
    val mutableCoupon: LiveData<Resource<Coupon>> get() = _mutableCoupon

    fun apiGetCity() {
        viewModelScope.launch {
            _mutableCity.postValue(Resource.loading(null))

            try {
                if (networkHelper.isNetworkConnected()) {
                    repositoryCommon.apiGetCity().let {
                        if (it.isSuccessful) {
                            _mutableCity.postValue(Resource.success(it.body() as City))
                        } else {
                            _mutableCity.postValue(Resource.error(it.errorBody().toString(), null))
                        }
                    }
                } else {
                    _mutableCity.postValue(
                        Resource.error(
                            resourceProvider.getString(R.string.no_internet_connection), null
                        )
                    )
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
                _mutableCity.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                _mutableCity.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }
        }
    }

    fun apiGetOffers() {
        viewModelScope.launch {

            _mutableCoupon.postValue(Resource.loading(null))

            try {
                if (networkHelper.isNetworkConnected()) {
                    repositoryCommon.apiGetOffers().let {
                        if (it.isSuccessful) {
                            var coupun: Coupon = it.body() as Coupon
                            _mutableCoupon.postValue(Resource.success(coupun))
                        } else {
                            _mutableCoupon.postValue(
                                Resource.error(
                                    it.errorBody().toString(), null
                                )
                            )
                        }
                    }
                } else {
                    _mutableCoupon.postValue(
                        Resource.error(
                            resourceProvider.getString(R.string.no_internet_connection), null
                        )
                    )
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
                _mutableCoupon.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                _mutableCoupon.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }
        }
    }
}