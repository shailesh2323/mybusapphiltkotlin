package com.natureinfotech.mybusapphilt.ui.first.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.model.GenerateOtp
import com.natureinfotech.mybusapphilt.ui.base.Activity_Base
import com.natureinfotech.mybusapphilt.ui.first.verify_otp.Activity_VerifyOtp
import com.natureinfotech.mybusapphilt.utils.Constant
import com.natureinfotech.mybusapphilt.utils.FieldsValidation
import com.natureinfotech.mybusapphilt.utils.PrintMessage
import com.natureinfotech.mybusapphilt.utils.Status
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_login.*

@AndroidEntryPoint
class Activity_Login : Activity_Base() {

    private val viewmodelLogin : ViewModel_Login by viewModels()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        onClickListner()
        setupObserver()
    }

    fun onClickListner() {
        btLogin.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when(view) {
            btLogin -> {
                val mobile : String = etUserNm.text.toString();

                if(FieldsValidation.isPhoneNumberValid(mobile)) {
                    val generateOtp : GenerateOtp = GenerateOtp()
                    generateOtp.mobileNumber = mobile
                    generateOtp.smsKey = "123456"

                    viewmodelLogin.apiGenerateOtp(generateOtp)
                }
                else {
                    PrintMessage.showErrorMessage(this,getString(R.string.enter_valid_mobile_number),id_lay_root);
                }
            }
        }
    }

    private fun setupObserver() {
        viewmodelLogin.generateOtp.observe(this, Observer {
            when(it.status) {
                Status.LOADING -> {
                    progressBar.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    progressBar.visibility = View.GONE
                    var generateOtp : GenerateOtp? =  it.data

                    val intent : Intent = Intent(this, Activity_VerifyOtp::class.java)
                    intent.putExtra(Constant.generateOtp,generateOtp);
                    startActivity(intent)
                    finish()
                }
                Status.ERROR -> {
                    progressBar.visibility = View.GONE
                }
            }
        })
    }
}

