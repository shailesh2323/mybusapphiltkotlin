package com.natureinfotech.mybusapphilt.ui.profile

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.databinding.ActivityProfileBinding
import com.natureinfotech.mybusapphilt.ui.base.Activity_Base
import com.natureinfotech.mybusapphilt.utils.DateFormatCls
import com.natureinfotech.mybusapphilt.utils.FileSelection
import com.natureinfotech.mybusapphilt.utils.progress_dialog.ProgressDialogCustom
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject


// Test Commit
@AndroidEntryPoint
class Activity_Profile : Activity_Base() {

    lateinit var binding : ActivityProfileBinding

    //@Inject lateinit var sharedPreferenceStorage: SharedPreferenceStorage

    //val viewModel : ViewModel_Profile by viewModels()

    lateinit var calendar : Calendar
    lateinit var mProgressDialog: ProgressDialogCustom

    private val RESULT_LOAD_IMAGE_Gallery = 402
    private val CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 401
    private val imageStoragePath: String? = null
    private val imageStorageByteArray = String()

    var isUniqueBoxOpen = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.activity_profile)

        calendar = Calendar.getInstance()

        /* Set Progress Bar */
        mProgressDialog = ProgressDialogCustom(this);
        mProgressDialog.setLoadImage(R.raw.progress_loader_gif);

        onInitilizer()
//        setContent()
//        onSetObservables()
//        onClickListner()
    }

    fun onInitilizer() {
        binding.radioGroup.setOnCheckedChangeListener { group, checkedId -> {
            if(checkedId == R.id.radiomale) {

            }
            else {

            }
        }}
    }

//    fun setContent() {
//        try {
//            val userName = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userName)
//            val userGender = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userGender)
//            val userMobile = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userMobile)
//            val userEmail = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userEmail)
//            val userDOB = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userDOB)
//            val userAge = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userAge)
//            val userUniqueCode = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userUniqueCode)
//            val routeId = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userRoleId)
//            val routeName = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userRouteName)
//            val villageId = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userVillageId)
//            val villageName = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userVillageName)
//            val areaId = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userAreaId)
//            val areaName = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userAreaName)
//            val profilePic = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userProfilePic)
//
//            try {
//                if (DateFormatCls.isDateValid(userDOB!!) && userDOB != "0000-00-00") {
//                    val date = DateFormatCls.inputFormat.parse(userDOB)
//                    calendar = Calendar.getInstance()
//                    calendar.time = date
//                }
//            } catch (e: Exception) {
//                e.printStackTrace()
//                calendar = Calendar.getInstance()
//            }
//
//            viewModel.mutableName.setValue(userName)
//            viewModel.mutableMobile.setValue(userMobile)
//            viewModel.mutableEmail.setValue(userEmail)
//            viewModel.mutableDOB.setValue(userDOB)
//            viewModel.mutableUniqueCode.setValue(userUniqueCode)
//            viewModel.mutableRouteId.setValue(routeId)
//            viewModel.mutableAreaId.setValue(areaId)
//            viewModel.mutableVillageId.setValue(villageId)
//
//            if (userGender!!.toLowerCase() == getString(R.string.male).toLowerCase()) {
//                binding.radiomale.isChecked = true
//                viewModel.mutableGender.setValue("1")
//            } else if (userGender!!.toLowerCase() == getString(R.string.female).toLowerCase()) {
//                binding.radiofemale.isChecked = true
//                viewModel.mutableGender.setValue("2")
//            } else {
//                binding.radiomale.isChecked = true
//                viewModel.mutableGender.setValue("1")
//            }
//            binding.tvVillageId.setText(villageName)
//            binding.tvAreaId.setText(areaName)
//            binding.tvRouteId.setText(routeName)
//            if (profilePic != "" && profilePic != null) {
//                FileSelection.setImagViewAsBitmapAndCircular(
//                    this@Activity_Profile,
//                    profilePic,
//                    binding.idImgProfileLogo
//                )
//            }
//        } catch (exception: Exception) {
//            exception.printStackTrace()
//        }
//    }

//    fun onSetObservables() {
//        // Routes
//        viewModel.mutableArrayListRoutes.observe(this, Observer {
//
//        })
//
//        // Villages
//        viewModel.mutableArrayListVillages.observe(this, Observer {
//
//        })
//
//        // Areas
//        viewModel.mutableArrayListAreas.observe(this, Observer {
//
//        })
//    }
//
//    fun onClickListner() {
//        binding.etDob.setOnClickListener(this)
//        binding.tvRouteId.setOnClickListener(this)
//        binding.tvVillageId.setOnClickListener(this)
//        binding.tvAreaId.setOnClickListener(this)
//        binding.tvCancel.setOnClickListener(this)
//        binding.idImgProfileLogo.setOnClickListener(this)
//    }

    override fun onClick(view: View?) {
        if(view == binding.etDob) {
            //var datePickerDialog = DatePickerDialog(t)
        }
    }
}