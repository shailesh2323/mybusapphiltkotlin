package com.natureinfotech.mybusapphilt.ui.profile

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.model.Areas
import com.natureinfotech.mybusapphilt.data.model.Routes
import com.natureinfotech.mybusapphilt.data.model.Villages
import com.natureinfotech.mybusapphilt.data.repository.Repository_Common
import com.natureinfotech.mybusapphilt.data.response.Profile
import com.natureinfotech.mybusapphilt.utils.NetworkHelper
import com.natureinfotech.mybusapphilt.utils.Resource
import com.natureinfotech.mybusapphilt.utils.resource_provider.ResourceProvider
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import kotlinx.coroutines.launch

class ViewModel_Profile @ViewModelInject constructor(
    private val repositoryCommon: Repository_Common,
    private val networkHelper: NetworkHelper,
    private val resourceProvider: ResourceProvider,
    private val sharedPreferenceStorage: SharedPreferenceStorage
) : ViewModel() {
    private val _mutableMessageProfile = MutableLiveData<Resource<String>>()
    val mutableMessageProfile get() = _mutableMessageProfile

    private val _mutableArrayListRoutes = MutableLiveData<Resource<List<Routes>>>()
    val mutableArrayListRoutes get() = _mutableArrayListRoutes

    private val _mutableArrayListVillages = MutableLiveData<Resource<List<Villages>>>()
    val mutableArrayListVillages get() = _mutableArrayListVillages

    private val _mutableArrayListAreas = MutableLiveData<Resource<List<Areas>>>()
    val mutableArrayListAreas get() = _mutableArrayListAreas

    fun apiGetProfile(profile : Profile.Data.User) {
        val hashMap = HashMap<String, String?>()
        hashMap["user_id"] = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.userId)
        hashMap["full_name"] = profile.full_name
        hashMap["email_id"] = profile.email_id
        hashMap["date_of_birth"] = profile.date_of_birth
        hashMap["gender"] = profile.gender
        hashMap["village_id"] = profile.village_id
        hashMap["area_id"] = profile.area_id
        hashMap["profile_pic"] = profile.profile_pic

        viewModelScope.launch {
            _mutableMessageProfile.postValue(Resource.loading(null))
            try {
                if(networkHelper.isNetworkConnected()) {
                    repositoryCommon.apiSaveProfile().let {
                        if(it.isSuccessful) {
                            var jsonObject = it.body() as JsonObject
                        }
                        else {
                            _mutableMessageProfile.postValue(Resource.error(it.errorBody().toString(),null))
                        }
                    }
                }
                else {
                    _mutableMessageProfile.postValue(Resource.error(resourceProvider.getString(R.string.no_internet_connection),null))
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
                _mutableMessageProfile.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                _mutableMessageProfile.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }
        }
    }

    fun apiGetRoute() {
        viewModelScope.launch {
            _mutableArrayListRoutes.postValue(Resource.loading(null))
            try {
                if(networkHelper.isNetworkConnected()) {
                    repositoryCommon.apiGetRoute().let {
                        if(it.isSuccessful) {
                            var jsonObject = it.body() as JsonObject
                        }
                        else {
                            _mutableArrayListRoutes.postValue(Resource.error(it.errorBody().toString(),null))
                        }
                    }
                }
                else {
                    _mutableArrayListRoutes.postValue(Resource.error(resourceProvider.getString(R.string.no_internet_connection),null))
                }
            } catch (exception: java.lang.Exception) {
                exception.printStackTrace()
                _mutableArrayListRoutes.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                _mutableArrayListRoutes.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }
        }
    }

    fun apiGetVillage(routeId : String) {
        var hashMap : HashMap<String,String> = HashMap<String,String>()
        hashMap["route_id"] = routeId

        viewModelScope.launch {
            _mutableArrayListVillages.postValue(Resource.loading(null))
            try {
                if(networkHelper.isNetworkConnected()) {
                    repositoryCommon.apiGetVillageByRoute(hashMap).let {
                        if(it.isSuccessful) {
                            var jsonObject = it.body() as JsonObject
                        }
                        else {
                            _mutableArrayListVillages.postValue(Resource.error(it.errorBody().toString(),null))
                        }
                    }
                }
                else {
                    _mutableArrayListVillages.postValue(Resource.error(resourceProvider.getString(R.string.no_internet_connection),null))
                }
            } catch (exception: java.lang.Exception) {
                exception.printStackTrace()
                _mutableArrayListVillages.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                _mutableArrayListVillages.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }
        }
    }

    fun apiGetArea() {
        viewModelScope.launch {
            _mutableArrayListAreas.postValue(Resource.loading(null))
            try {
                if(networkHelper.isNetworkConnected()) {
                    repositoryCommon.apiGetArea().let {
                        if(it.isSuccessful) {
                            var jsonObject = it.body() as JsonObject
                        }
                        else {
                            _mutableArrayListAreas.postValue(Resource.error(it.errorBody().toString(),null))
                        }
                    }
                }
                else {
                    _mutableArrayListAreas.postValue(Resource.error(resourceProvider.getString(R.string.no_internet_connection),null))
                }
            } catch (exception: java.lang.Exception) {
                exception.printStackTrace()
                _mutableArrayListAreas.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                _mutableArrayListAreas.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }
        }
    }

}