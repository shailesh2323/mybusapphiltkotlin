package com.natureinfotech.mybusapphilt.ui.first.verify_otp

import android.content.res.Resources
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.model.GenerateOtp
import com.natureinfotech.mybusapphilt.data.repository.Repository_Login
import com.natureinfotech.mybusapphilt.data.response.VerifyOtp
import com.natureinfotech.mybusapphilt.utils.JWTUtils
import com.natureinfotech.mybusapphilt.utils.NetworkHelper
import com.natureinfotech.mybusapphilt.utils.Resource
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import kotlinx.coroutines.launch
import javax.inject.Inject


class ViewModel_VerifyOtp @ViewModelInject constructor(private val repositoryLogin : Repository_Login,
                                                       private val newtworkHelper : NetworkHelper,
                                                       private val sharedPreferenceStorage : SharedPreferenceStorage) : ViewModel() {

    private val _verifyOtp = MutableLiveData<Resource<Boolean>>()
    val verifyOtp : LiveData<Resource<Boolean>> get() = _verifyOtp

    private val _generateOtp = MutableLiveData<Resource<GenerateOtp>>()
    val generateOtp : LiveData<Resource<GenerateOtp>> get() = _generateOtp

    fun apiVerifyOtp(generateOtp : GenerateOtp) {

        val hashMap : HashMap<String,String> = HashMap<String,String>()
        hashMap.put("full_name",generateOtp.name)
        hashMap.put("mobile_number",generateOtp.mobileNumber)
        hashMap.put("email_id",generateOtp.email)
        hashMap.put("date_of_birth",generateOtp.dob)
        hashMap.put("gender",generateOtp.gender)
        hashMap.put("is_social",generateOtp.isSocial);
        hashMap.put("role_id",generateOtp.roleId);
        hashMap.put("otp",generateOtp.otp);

        viewModelScope.launch {

            _verifyOtp.postValue(Resource.loading(null))

            if(newtworkHelper.isNetworkConnected()) {
                repositoryLogin.apiVerifyOtp(hashMap).let {

                    if(it.isSuccessful) {
                        var verifyOtp : VerifyOtp = it.body() as VerifyOtp

                        var data : VerifyOtp.Data = verifyOtp.data

                        var token : String? = data.token

                        var body : String = JWTUtils.decoded(token)

                        var bodyJson : JsonObject = JsonParser().parse(body).asJsonObject

                        sharedPreferenceStorage.addInformation(sharedPreferenceStorage.userId,bodyJson.get("user_id").asString)
                        sharedPreferenceStorage.addInformation(sharedPreferenceStorage.userMobile,bodyJson.get("mobile_number").asString)
                        sharedPreferenceStorage.addInformation(sharedPreferenceStorage.userRoleId,bodyJson.get("role_id").asString)
                        sharedPreferenceStorage.addInformation(sharedPreferenceStorage.authToken,token)

                        _verifyOtp.postValue(Resource.success(true))
                    }
                    else {
                        _verifyOtp.postValue(Resource.error(it.errorBody().toString(),null))
                    }
                }
            }
            else {
                _verifyOtp.postValue(Resource.error(Resources.getSystem().getString(R.string.no_internet_connection),null))
            }
        }
    }

    public fun apiGenerateOtp(generateOtp: GenerateOtp) {
        val hashMap : HashMap<String,String> = HashMap<String,String>()
        hashMap.put("sms_key",generateOtp.smsKey);
        hashMap.put("mobile_number",generateOtp.mobileNumber);

        viewModelScope.launch {
            _generateOtp.postValue(Resource.loading(null));

            if(newtworkHelper.isNetworkConnected()) {
                repositoryLogin.apiGeneratedOtp(hashMap).let {
                    if(it.isSuccessful) {
                        _generateOtp.postValue(Resource.success(generateOtp))
                    }
                    else {
                        _generateOtp.postValue(Resource.error(it.errorBody().toString(),null))
                    }
                }
            }
            else {
                _generateOtp.postValue(Resource.error(Resources.getSystem().getString(R.string.no_internet_connection),null))
            }
        }
    }
}

















