package com.natureinfotech.mybusapphilt.ui.dashboard.home

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer;
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.utils.DateFormatCls
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import dagger.hilt.android.AndroidEntryPoint
import androidx.fragment.app.viewModels
import com.natureinfotech.mybusapphilt.data.response.City
import com.natureinfotech.mybusapphilt.databinding.FragmentHomeBinding
import com.natureinfotech.mybusapphilt.ui.bus.bus_list.Activity_Bus_List
import com.natureinfotech.mybusapphilt.utils.PrintMessage
import com.natureinfotech.mybusapphilt.utils.Status
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.json.JSONObject
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

@AndroidEntryPoint
class Fragment_Home : Fragment(), View.OnClickListener {

    val viewModel : ViewModel_Home by viewModels()

    lateinit var binding : FragmentHomeBinding

    @Inject lateinit var sharedPreferenceStorage: SharedPreferenceStorage

    lateinit var idEditDate : EditText
    lateinit var btLogin : Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding =  DataBindingUtil.inflate(inflater,R.layout.fragment_home, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onClickListner()
        onSetObservables()

        viewModel.apiGetCity()
        viewModel.apiGetOffers()
    }

    fun onClickListner() {
        var returnMessage = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.returnJourneyMessage)

        if(!returnMessage.equals("")) {
            id_check_return_journey.setText(returnMessage)
        }

        binding.idEditDate.setOnClickListener(this)
        binding.btLogin.setOnClickListener(this)
    }

    override fun onClick(view : View?) {

        when(view?.id) {
            R.id.id_edit_date -> {
                val calender = Calendar.getInstance()

                val year = calender.get(Calendar.YEAR)
                val month = calender.get(Calendar.MONTH)
                val day_of_month = calender.get(Calendar.DAY_OF_MONTH)

                val datePickerDialog = DatePickerDialog(
                    requireActivity(),
                    OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                        var calendar : Calendar = Calendar.getInstance()
                        calendar.set(year,monthOfYear,dayOfMonth)

                        val date: String = DateFormatCls.inputFormat.format(calendar.getTime())
                        try {
                            id_edit_date.setText(date)
                        } catch (e: Exception) {
                            e.stackTrace
                        }
                    }, year, month, day_of_month)

                datePickerDialog.datePicker.minDate = calender.timeInMillis
                datePickerDialog.datePicker.maxDate = ((calender.timeInMillis) + (1000*60*60*24*24))
                datePickerDialog.show()
            }

            R.id.btLogin -> {
                var source : String = binding.idEditSource.text.toString()
                var destination : String = binding.idEditDestination.text.toString()
                var date : String = binding.idEditDate.text.toString()

                if(source.equals("")) {
                    PrintMessage.showErrorMessage(requireContext(),getString(R.string.please_enter_source),binding.idLayRoot)
                }
                else if(destination.equals("")) {
                    PrintMessage.showErrorMessage(requireContext(),getString(R.string.please_enter_destination),binding.idLayRoot)
                }
                else if(date.equals("")) {
                    PrintMessage.showErrorMessage(requireContext(),getString(R.string.please_enter_date),binding.idLayRoot)
                }
                else {
                    var intent : Intent = Intent(requireActivity(),Activity_Bus_List::class.java)
                    intent.putExtra("source",source)
                    intent.putExtra("destination",destination)
                    intent.putExtra("date",date)

                    startActivity(intent)
                }
            }
        }
    }

    fun onSetObservables() {
        // mutableCity
        viewModel.mutableCity.observe(viewLifecycleOwner, Observer {
            when(it.status) {
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }
                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE

                    var city : City = it.data as City

                    var list : List<City.Data.Cities> = city.data.cities
                    adapterSettingEnterSource(list)
                    adapterSettingEnterDestination(list)
                }
                Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                }
            }
        })

        // mutableCoupon
        viewModel.mutableCoupon.observe(viewLifecycleOwner, Observer {
            when(it.status) {
                Status.LOADING -> {
                    binding.progressBar.visibility = View.VISIBLE
                }

                Status.SUCCESS -> {
                    binding.progressBar.visibility = View.GONE
                }

                Status.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                }
            }
        })
    }

    fun adapterSettingEnterSource(list: List<City.Data.Cities>) {
        val adapter = ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, list)
        binding.idEditSource.setAdapter(adapter)

        binding.idEditSource.setOnItemClickListener(AdapterView.OnItemClickListener { parent, view, position, id ->
            val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken,0)
        })
    }

    fun adapterSettingEnterDestination(list: List<City.Data.Cities>) {
        val adapter = ArrayAdapter(requireContext(),android.R.layout.simple_list_item_1,list)
        binding.idEditDestination.setAdapter(adapter)

        binding.idEditDestination.setOnItemClickListener(AdapterView.OnItemClickListener { parent, view, position, id ->
            val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken,0)
        })
    }
}