package com.natureinfotech.mybusapphilt.ui.webview

import android.app.Activity
import android.graphics.Bitmap
import android.webkit.WebView
import android.webkit.WebViewClient
import com.natureinfotech.mybusapphilt.utils.progress_dialog.ProgressDialogCustom


class WebViewClientImpl : WebViewClient {
    private var activity: Activity? = null

    var mProgressDialog: ProgressDialogCustom? = null

    constructor(
        activity: Activity?,
        mProgressDialog: ProgressDialogCustom?
    ) {
        this.activity = activity
        this.mProgressDialog = mProgressDialog
    }

    override fun shouldOverrideUrlLoading(webView: WebView?, url: String?): Boolean {
//        if(url.indexOf("journaldev.com") > -1 ) return false;
//
//        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
//        activity.startActivity(intent);
        return true
    }

    override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
        super.onPageStarted(view, url, favicon)
        mProgressDialog!!.show()
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        mProgressDialog!!.dismiss()
    }
}