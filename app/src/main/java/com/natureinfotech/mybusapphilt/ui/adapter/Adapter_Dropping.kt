package com.natureinfotech.mybusapphilt.ui.adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.response.BusList
import com.natureinfotech.mybusapphilt.databinding.RowDroppingBinding
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Adapter_Dropping @Inject constructor(@ApplicationContext var context : Context) : RecyclerView.Adapter<Adapter_Dropping.ViewHolder>() {

    lateinit var alreadySelect : String
    lateinit var default_price : String

    private val callBack = object : DiffUtil.ItemCallback<BusList.Data.Droppings>() {
        override fun areItemsTheSame(
            oldItem: BusList.Data.Droppings,
            newItem: BusList.Data.Droppings
        ): Boolean {
            return oldItem.bus_dropping_id == newItem.bus_dropping_id
        }

        override fun areContentsTheSame(
            oldItem: BusList.Data.Droppings,
            newItem: BusList.Data.Droppings
        ): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this,callBack)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var inflator : LayoutInflater = LayoutInflater.from(parent.context)
        var binding : RowDroppingBinding = DataBindingUtil.inflate(inflator, R.layout.row_dropping,parent,false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var dropping = differ.currentList[position]
        holder.bind(dropping)
    }

    inner class ViewHolder constructor(var binding : RowDroppingBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(dropping : BusList.Data.Droppings) {
            binding.tvName.text = dropping.name;
            binding.tvPlaceName.text = dropping.address;

            if (default_price == "2") {
                binding.tvPrice.visibility = View.VISIBLE
                binding.tvOriginalPrice.visibility = View.VISIBLE
                binding.tvOriginalPrice.text = context.getString(R.string.rupee_symbol) + " " + dropping.original_price
                binding.tvOriginalPrice.paintFlags = binding.tvOriginalPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                binding.tvPrice.text = context.getString(R.string.rupee_symbol) + " " + dropping.discount_price
            } else {
                binding.tvPrice.visibility = View.GONE
                binding.tvOriginalPrice.visibility = View.GONE
            }

            if(alreadySelect == dropping.address) {
                binding.idRadioButton.isChecked = true
                binding.idViewSelection.setBackgroundColor(context.resources.getColor(R.color.colorPrimaryBlue))
            }

            binding.idLayMain.setOnClickListener(View.OnClickListener {
                binding.idRadioButton.isChecked = true
                binding.idViewSelection.setBackgroundColor(context.resources.getColor(R.color.colorPrimaryBlue))
                onItemClickListner?.let {
                    it(dropping)
                }
            })

            binding.idRadioButton.setOnClickListener(View.OnClickListener {
                binding.idRadioButton.isChecked = true
                binding.idViewSelection.setBackgroundColor(context.resources.getColor(R.color.colorPrimaryBlue))
                onItemClickListner?.let {
                    it(dropping)
                }
            })
        }
    }

    fun getTheInformation(alreadySelect : String, default_price : String) {
        this.alreadySelect = alreadySelect
        this.default_price = default_price
    }

    private var onItemClickListner : ((BusList.Data.Droppings) -> Unit)? = null

    fun setOnItemClickListner(listner : (BusList.Data.Droppings) -> Unit) {
        onItemClickListner = listner
    }
}












