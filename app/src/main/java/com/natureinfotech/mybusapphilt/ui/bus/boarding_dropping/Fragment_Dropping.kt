package com.natureinfotech.mybusapphilt.ui.bus.boarding_dropping

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.response.BusList
import com.natureinfotech.mybusapphilt.databinding.FragmentDroppingBinding
import com.natureinfotech.mybusapphilt.ui.adapter.Adapter_Dropping
import com.natureinfotech.mybusapphilt.ui.bus.bus_passenger_info.Activity_PassengerInfo
import com.natureinfotech.mybusapphilt.utils.PrintMessage
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class Fragment_Dropping : Fragment() {

    lateinit var binding : FragmentDroppingBinding
    lateinit var bus : BusList.Data

    @Inject lateinit var sharedPreferenceStorage: SharedPreferenceStorage
    @Inject lateinit var adapter : Adapter_Dropping

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_dropping, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bus = arguments?.getSerializable("bus") as BusList.Data
        setRecyclerView()
    }

    fun setRecyclerView() {
        var alredySelectedDropping = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectUserDropping)

        var arrayList : List<BusList.Data.Droppings> = bus.droppings

        if(arrayList.isNotEmpty()) {
            binding.idRecycleDropping.adapter = adapter
            var layoutManager : RecyclerView.LayoutManager = LinearLayoutManager(this.context,LinearLayoutManager.VERTICAL,false)
            binding.idRecycleDropping.layoutManager = layoutManager
            adapter.differ.submitList(arrayList)
            adapter.getTheInformation(alredySelectedDropping!!,bus.default_price)

            adapter.setOnItemClickListner {
                var dropping = it as BusList.Data.Droppings

                sharedPreferenceStorage.addInformation(sharedPreferenceStorage.selectUserDropping,dropping.address)

                if(bus.default_price == "2") {
                    var strJson = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectPassengerInfo)
                    var jsonElement = JsonParser().parse(strJson)
                    var jsonArrayPassengerInfo = jsonElement.asJsonArray

                    for(index in 0 until jsonArrayPassengerInfo.size()) {
                        var jsonObject : JsonObject = jsonArrayPassengerInfo.get(index).asJsonObject
                        jsonObject.addProperty("original_price",dropping.original_price)
                        jsonObject.addProperty("discount_price",dropping.discount_price)
                    }
                    sharedPreferenceStorage.addInformation(sharedPreferenceStorage.selectPassengerInfo,jsonArrayPassengerInfo.toString());
                }

                val alredySelectedBording = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectUserBoarding)
                val alredySelectedDropping = sharedPreferenceStorage.getInformation(sharedPreferenceStorage.selectUserDropping)

                if(alredySelectedBording == "") {
                    PrintMessage.showErrorMessage(this.requireContext(),getString(R.string.please_select_boarding),binding.idLayRoot)
                }
                else if(alredySelectedDropping == "") {
                    PrintMessage.showErrorMessage(this.requireContext(),getString(R.string.please_select_dropping),binding.idLayRoot)
                }
                else {
                    var intent : Intent = Intent(requireActivity(),Activity_PassengerInfo::class.java)
                    startActivity(intent)
                }
            }
        }
        else {
            binding.idRecycleDropping.visibility = View.GONE
            binding.idTxtNoDataFound.visibility = View.VISIBLE
        }
    }
}