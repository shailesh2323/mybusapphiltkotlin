package com.natureinfotech.mybusapphilt.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.model.PassengerInfo
import com.natureinfotech.mybusapphilt.databinding.RowPassengerInfoBinding
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Adapter_Passenger_Info @Inject constructor(@ApplicationContext var context : Context) :
    RecyclerView.Adapter<Adapter_Passenger_Info.ViewHolder>() {

    private val callBack = object : DiffUtil.ItemCallback<PassengerInfo>() {
        override fun areItemsTheSame(
            oldItem: PassengerInfo,
            newItem: PassengerInfo
        ): Boolean {
            return oldItem.bus_seat_transaction_id == newItem.bus_seat_transaction_id
        }

        override fun areContentsTheSame(
            oldItem: PassengerInfo,
            newItem: PassengerInfo
        ): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this,callBack)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var inflater : LayoutInflater = LayoutInflater.from(parent.context)
        var rowPassengerInfoBinding : RowPassengerInfoBinding = DataBindingUtil.inflate(inflater, R.layout.row_passenger_info, parent, false)
        return ViewHolder(rowPassengerInfoBinding)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var passengerInfo = differ.currentList[position]
        holder.bind(passengerInfo)
    }

    inner class ViewHolder constructor(var binding : RowPassengerInfoBinding) : RecyclerView.ViewHolder(binding.root)
    {
        fun bind(passengerInfo : PassengerInfo) {
            binding.tvSeatNo.setText(context.getString(R.string.seat)+" "+passengerInfo.number)
            try {
                binding.etName.setText(passengerInfo.name);
                binding.etAge.setText(passengerInfo.age);

                var gender = passengerInfo.gender

                if(gender.equals("1")) {
                    binding.radiomale.setChecked(true);
                }
                else {
                    binding.radiofemale.setChecked(true);
                }
            }
            catch (exception : Exception) {
                exception.printStackTrace();
            }
        }
    }
}