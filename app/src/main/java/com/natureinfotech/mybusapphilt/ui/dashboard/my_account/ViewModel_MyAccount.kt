package com.natureinfotech.mybusapphilt.ui.dashboard.my_account

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import com.natureinfotech.mybusapphilt.R
import com.natureinfotech.mybusapphilt.data.repository.Repository_Common
import com.natureinfotech.mybusapphilt.data.response.Profile
import com.natureinfotech.mybusapphilt.utils.NetworkHelper
import com.natureinfotech.mybusapphilt.utils.Resource
import com.natureinfotech.mybusapphilt.utils.resource_provider.ResourceProvider
import com.natureinfotech.mybusapphilt.utils.shared_preference.SharedPreferenceStorage
import kotlinx.coroutines.launch
import java.lang.Exception


class ViewModel_MyAccount @ViewModelInject constructor(
    private val respositoryCommon: Repository_Common,
    private val networkHelper: NetworkHelper,
    private val resourceProvider: ResourceProvider,
    private val sharedPreferenceStorage: SharedPreferenceStorage
) : ViewModel() {

    private val _mutableComplete = MutableLiveData<Resource<Profile>>()
    val mutableComplete get() = _mutableComplete

    fun apiGetProfile() {

        viewModelScope.launch {
            _mutableComplete.postValue(Resource.loading(null))
            try {
                if(networkHelper.isNetworkConnected()) {
                    respositoryCommon.apiGetProfile().let {
                        if(it.isSuccessful) {
                            var profile : Profile = it.body() as Profile

                            var user : Profile.Data.User = profile.data.user

                            var gson = Gson()
                            var jsonStringUser = gson.toJson(user)
                            var jsonObjectUser = JsonParser().parse(jsonStringUser).asJsonObject

                            sharedPreferenceStorage.saveProfile(jsonObjectUser)

                            _mutableComplete.postValue(Resource.success(profile))
                        }
                        else {
                            _mutableComplete.postValue(Resource.error(it.errorBody().toString(),null))
                        }
                    }
                }
                else {
                    _mutableComplete.postValue(Resource.error(resourceProvider.getString(R.string.no_internet_connection),null))
                }
            } catch (exception: Exception) {
                exception.printStackTrace()
                _mutableComplete.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            } catch (throwable: Throwable) {
                throwable.printStackTrace()
                _mutableComplete.postValue(
                    Resource.error(
                        resourceProvider.getString(R.string.something_went_wrong), null
                    )
                )
            }
        }
    }
}