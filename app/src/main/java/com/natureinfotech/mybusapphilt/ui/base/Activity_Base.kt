package com.natureinfotech.mybusapphilt.ui.base

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity

open class Activity_Base : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onClick(view : View?) {
        TODO("Not yet implemented")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}