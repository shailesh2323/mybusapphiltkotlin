package com.natureinfotech.mybusapphilt.data.model

import java.io.Serializable

class Villages(
    var village_id : String,
    var route_id : String,
    var name : String,
    var code : String
) : Serializable