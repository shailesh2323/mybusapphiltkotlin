package com.natureinfotech.mybusapphilt.data.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class GenerateOrderId(@SerializedName("data") var data: Data) : Serializable {
    data class Data(
        @SerializedName("id") var id: String,
        @SerializedName("entity") var entity: String,
        @SerializedName("amount") var amount: Int,
        @SerializedName("amount_paid") var amount_paid: Int,
        @SerializedName("amount_due") var amount_due: Int,
        @SerializedName("currency") var currency: String,
        @SerializedName("receipt") var receipt: String,
        @SerializedName("offer_id") var offer_id: String,
        @SerializedName("status") var status: String,
        @SerializedName("attempts") var attempts: Int,
        @SerializedName("notes") var notes: List<String>,
        @SerializedName("created_at") var created_at: Int,
        @SerializedName("razorpay_key") var razorpay_razorpay_key: String
    ) : Serializable
}