package com.natureinfotech.mybusapphilt.data.api

import com.google.gson.JsonObject
import com.natureinfotech.mybusapphilt.data.response.*
import org.json.JSONObject
import retrofit2.Response
import javax.inject.Inject

class ApiHelperImpl @Inject constructor(private val apiService: ApiService) : ApiHelper {

    override suspend fun apiCheckVersion(hashMap: HashMap<String,String>): Response<CheckVersion> = apiService.apiCheckVersion(hashMap)

    override suspend fun apiGeneratedOtp(hashMap: HashMap<String,String>): Response<JsonObject> = apiService.apiGeneratedOtp(hashMap)

    override suspend fun apiVerifyOtp(hashMap: HashMap<String, String>): Response<VerifyOtp> = apiService.apiVerifyOtp(hashMap)

    override suspend fun apiGetCity(): Response<City> = apiService.apiGetCity()

    override suspend fun apiGetOffers(): Response<Coupon> = apiService.apiGetOffers()

    override suspend fun apiGetBusList(hashMap: HashMap<String, String>): Response<BusList> = apiService.apiGetBusList(hashMap)

    override suspend fun apiGenerateOrderId(hashMap: HashMap<String, String>): Response<GenerateOrderId> = apiService.apiGenerateOrderId(hashMap)

    override suspend fun apiValidateCoupon(hashMap: HashMap<String, String>): Response<JsonObject> = apiService.apiValidateCoupon(hashMap)

    override suspend fun apiSeatBooking(hashMap: HashMap<String, Any?>): Response<JsonObject> = apiService.apiSeatBooking(hashMap)

    override suspend fun apiGetProfile(): Response<Profile> = apiService.apiGetProfile()

    override suspend fun apiSaveProfile(): Response<JsonObject> = apiService.apiSaveProfile()

    override suspend fun apiGetRoute(): Response<JsonObject> = apiService.apiGetRoute()

    override suspend fun apiGetVillageByRoute(hashMap: HashMap<String, String>) : Response<JsonObject> = apiService.apiGetVillageByRoute(hashMap)

    override suspend fun apiGetArea(): Response<JsonObject> = apiService.apiGetArea()

    override suspend fun apiGenerateUnicode(hashMap: HashMap<String, String>): Response<JsonObject> = apiService.apiGenerateUnicode(hashMap)
}