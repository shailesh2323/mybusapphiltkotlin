package com.natureinfotech.mybusapphilt.data.repository

import com.google.gson.JsonObject
import com.natureinfotech.mybusapphilt.data.api.ApiHelper
import retrofit2.Response
import javax.inject.Inject

class Repository_Bus @Inject constructor(private val apiHelper: ApiHelper) {
    suspend fun apiGetBusList(hashMap: HashMap<String,String>) = apiHelper.apiGetBusList(hashMap)

    suspend fun apiGenerateOrderId(hashMap: HashMap<String, String>) = apiHelper.apiGenerateOrderId(hashMap)

    suspend fun apiValidateCoupon(hashMap: HashMap<String, String>) = apiHelper.apiValidateCoupon(hashMap)

    suspend fun apiSeatBooking(hashMap: HashMap<String, Any?>) = apiHelper.apiSeatBooking(hashMap)
}