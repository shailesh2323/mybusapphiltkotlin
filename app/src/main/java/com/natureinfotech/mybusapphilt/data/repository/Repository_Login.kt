package com.natureinfotech.mybusapphilt.data.repository

import com.natureinfotech.mybusapphilt.data.api.ApiHelper
import javax.inject.Inject

class Repository_Login @Inject constructor(private val apiHelper: ApiHelper) {
    suspend fun apiCheckVersion(hashMap: HashMap<String,String>) = apiHelper.apiCheckVersion(hashMap)

    suspend fun apiGeneratedOtp(hashMap: HashMap<String,String>) = apiHelper.apiGeneratedOtp(hashMap)

    suspend fun apiVerifyOtp(hashMap: HashMap<String, String>) = apiHelper.apiVerifyOtp(hashMap)
}