package com.natureinfotech.mybusapphilt.data.response

import com.google.gson.annotations.SerializedName

data class City(@SerializedName("data") val data : Data) {
    data class Data (
        @SerializedName("cities") val cities : List<Cities>
    ) {
        data class Cities (
            @SerializedName("city_id") val city_id : Int,
            @SerializedName("name") val name : String
        ) {
            override fun toString(): String {
                return name
            }
        }
    }
}