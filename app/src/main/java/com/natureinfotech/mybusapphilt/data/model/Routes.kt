package com.natureinfotech.mybusapphilt.data.model

import java.io.Serializable

class Routes(
    var route_id : String,
    var name : String,
    var code : String
) : Serializable