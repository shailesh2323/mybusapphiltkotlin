package com.natureinfotech.mybusapphilt.data.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class BusList(
    @SerializedName("data") val data : List<Data>
) : Serializable {
    data class Data (
        @SerializedName("bus_transaction_id") val bus_transaction_id : String,
        @SerializedName("bus_id") val bus_id : String,
        @SerializedName("travelling_date") val travelling_date : String,
        @SerializedName("agent_number") val agent_number : String,
        @SerializedName("live_url") val live_url : String,
        @SerializedName("is_active") val is_active : String,
        @SerializedName("name") val name : String,
        @SerializedName("brand") val brand : String,
        @SerializedName("number") val number : String,
        @SerializedName("original_price") val original_price : Double,
        @SerializedName("discount_price") val discount_price : Double,
        @SerializedName("original_group_price") val original_group_price : Double,
        @SerializedName("discount_group_price") val discount_group_price : Double,
        @SerializedName("default_price") val default_price : String,
        @SerializedName("total_seats") val total_seats : String,
        @SerializedName("start_city_id") val start_city_id : String,
        @SerializedName("end_city_id") val end_city_id : String,
        @SerializedName("type") val type : String,
        @SerializedName("isactive") val isactive : String,
        @SerializedName("created_at") val created_at : String,
        @SerializedName("updated_at") val updated_at : String,
        @SerializedName("city_id") val city_id : String,
        @SerializedName("start_city") val start_city : String,
        @SerializedName("end_city") val end_city : String,
        @SerializedName("seats") val seats : List<List<Seats>>,
        @SerializedName("boardings") val boardings : List<Boardings>,
        @SerializedName("droppings") val droppings : List<Droppings>
    ) : Serializable {
        data class Droppings (
            @SerializedName("bus_dropping_id") val bus_dropping_id : String,
            @SerializedName("bus_id") val bus_id : String,
            @SerializedName("dropping_id") val dropping_id : String,
            @SerializedName("dropping_time") val dropping_time : String,
            @SerializedName("original_price") val original_price : Double,
            @SerializedName("discount_price") val discount_price : Double,
            @SerializedName("dropping_order") val dropping_order : String,
            @SerializedName("name") val name : String,
            @SerializedName("address") val address : String,
            @SerializedName("mobile_numbers") val mobile_numbers : String
        ) : Serializable

        data class Boardings (
            @SerializedName("bus_boarding_id") val bus_boarding_id : String,
            @SerializedName("bus_id") val bus_id : String,
            @SerializedName("boarding_id") val boarding_id : String,
            @SerializedName("boarding_time") val boarding_time : String,
            @SerializedName("original_price") val original_price : Double,
            @SerializedName("discount_price") val discount_price : Double,
            @SerializedName("boarding_order") val boarding_order : String,
            @SerializedName("name") val name : String,
            @SerializedName("address") val address : String,
            @SerializedName("mobile_numbers") val mobile_numbers : String
        ) : Serializable

        data class Seats (
            @SerializedName("bus_seat_transaction_id") val bus_seat_transaction_id : String,
            @SerializedName("bus_transaction_id") val bus_transaction_id : String,
            @SerializedName("status") val status : String,
            @SerializedName("number") val number : String
        ) : Serializable
    }
}