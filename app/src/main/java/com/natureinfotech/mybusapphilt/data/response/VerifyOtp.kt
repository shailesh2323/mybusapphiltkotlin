package com.natureinfotech.mybusapphilt.data.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class VerifyOtp(@SerializedName("data") val data : Data) : Serializable {
    data class Data (
        @SerializedName("token") val token : String,
        @SerializedName("coupons") val coupons : List<Coupons>
    ) {
        data class Coupons (
            @SerializedName("coupon_id") val coupon_id : Int,
            @SerializedName("coupon_type_id") val coupon_type_id : Int,
            @SerializedName("coupon_code") val coupon_code : String,
            @SerializedName("coupon_type_name") val coupon_type_name : String,
            @SerializedName("description") val description : String,
            @SerializedName("off_value") val off_value : Int,
            @SerializedName("is_percentage") val is_percentage : Int,
            @SerializedName("created_on") val created_on : String,
            @SerializedName("expired_on") val expired_on : String,
            @SerializedName("min_value") val min_value : Int,
            @SerializedName("max_used") val max_used : Int,
            @SerializedName("is_active") val is_active : Int
        )
    }
}



