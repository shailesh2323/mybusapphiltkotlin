package com.natureinfotech.mybusapphilt.data.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Profile(
    @SerializedName("data") val data : Data
) : Serializable {
    data class Data (
        @SerializedName("user") val user : User
    ) {
        data class User (
            @SerializedName("user_id") val user_id : String,
            @SerializedName("full_name") val full_name : String,
            @SerializedName("mobile_number") val mobile_number : String,
            @SerializedName("email_id") val email_id : String,
            @SerializedName("profile_pic") val profile_pic : String,
            @SerializedName("date_of_birth") val date_of_birth : String,
            @SerializedName("gender") val gender : String,
            @SerializedName("role_id") val role_id : String,
            @SerializedName("unicode") val unicode : String,
            @SerializedName("route_id") val route_id : String,
            @SerializedName("route_code") val route_code : String,
            @SerializedName("route_name") val route_name : String,
            @SerializedName("village_id") val village_id : String,
            @SerializedName("village_name") val village_name : String,
            @SerializedName("village_code") val village_code : String,
            @SerializedName("area_id") val area_id : String,
            @SerializedName("area_name") val area_name : String,
            @SerializedName("area_code") val area_code : String,
            @SerializedName("return_message") val return_message : String
        )
    }
}