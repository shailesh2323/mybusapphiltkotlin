package com.natureinfotech.mybusapphilt.data.api

class ApiClient {

    companion object {
        /*
         * TODO UAT
         * */
        val Base_Url = "https://raigadshree-uat-api.herokuapp.com/"
        val Base_Url_WebView = "https://raigadshree-uat.natureinfotech.com/"

        val aboutUs = Base_Url_WebView + "webview/about-us"
        val referToEarn = Base_Url_WebView + "webview/refer-to-earn"
        val help = Base_Url_WebView + "webview/help"
        val offers = Base_Url_WebView + "webview/offers"
        val callSupport = Base_Url_WebView + "webview/call-support"
        val privacyPolicy = Base_Url_WebView + "auth/privacy-policy"

        val invoiceLink = Base_Url_WebView + "webview/generate-invoice?receipt_number="

        val smartCardLink = Base_Url_WebView + "webview/generate-smart-card?mobile="
    }

}