package com.natureinfotech.mybusapphilt.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class PassengerInfo(
    @SerializedName("bus_seat_transaction_id") var bus_seat_transaction_id : Int,
    @SerializedName("bus_transaction_id") var bus_transaction_id : Int,
    @SerializedName("number") var number : String,
    @SerializedName("status") var status : Int,
    @SerializedName("seatStatus") var seatStatus : Int,
    @SerializedName("original_price") var original_price : Double,
    @SerializedName("discount_price") var discount_price : Double,
    @SerializedName("name") var name : String,
    @SerializedName("age") var age : String,
    @SerializedName("gender") var gender : String
) : Serializable