package com.natureinfotech.mybusapphilt.data.response

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class CheckVersion  (@SerializedName("data") var data : Data) : Serializable {
    class Data(@SerializedName("version") var version: String?,
               @SerializedName("url") var url: String?,
               @SerializedName("description") var description: String?,
               @SerializedName("stable_version") var stable_version: Boolean?)
}




