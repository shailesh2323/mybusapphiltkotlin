package com.natureinfotech.mybusapphilt.data.api

import com.google.gson.JsonObject
import com.natureinfotech.mybusapphilt.data.response.*
import org.json.JSONObject
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiHelper {
    suspend fun apiCheckVersion(hashMap: HashMap<String,String>) : Response<CheckVersion>

    suspend fun apiGeneratedOtp(hashMap: HashMap<String,String>) : Response<JsonObject>

    suspend fun apiVerifyOtp(hashMap: HashMap<String, String>) : Response<VerifyOtp>

    suspend fun apiGetCity() : Response<City>

    suspend fun apiGetOffers() : Response<Coupon>

    suspend fun apiGetBusList(hashMap: HashMap<String, String>) : Response<BusList>

    suspend fun apiGenerateOrderId(hashMap: HashMap<String, String>) : Response<GenerateOrderId>

    suspend fun apiValidateCoupon(hashMap: HashMap<String, String>) : Response<JsonObject>

    suspend fun apiSeatBooking(hashMap: HashMap<String, Any?>) : Response<JsonObject>

    suspend fun apiGetProfile() : Response<Profile>

    suspend fun apiSaveProfile() : Response<JsonObject>

    suspend fun apiGetRoute() : Response<JsonObject>

    suspend fun apiGetVillageByRoute(hashMap: HashMap<String, String>) : Response<JsonObject>

    suspend fun apiGetArea() : Response<JsonObject>

    suspend fun apiGenerateUnicode(hashMap: HashMap<String, String>) : Response<JsonObject>
}