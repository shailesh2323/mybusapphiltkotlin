package com.natureinfotech.mybusapphilt.data.response

import com.google.gson.annotations.SerializedName

class Coupon(
    @SerializedName("data") val data : Data
) {
    class Data(
        @SerializedName("coupons") val coupons : List<Coupons>
    ) {
        class Coupons(
            @SerializedName("coupon_id") val coupon_id : String,
            @SerializedName("coupon_type_id") val coupon_type_id : String,
            @SerializedName("coupon_code") val coupon_code : String,
            @SerializedName("coupon_type_name") val coupon_type_name : String,
            @SerializedName("name") val name : String,
            @SerializedName("description") val description : String,
            @SerializedName("off_value") val off_value : String,
            @SerializedName("is_percentage") val is_percentage : String,
            @SerializedName("created_on") val created_on : String,
            @SerializedName("expired_on") val expired_on : String,
            @SerializedName("min_value") val min_value : String,
            @SerializedName("max_used") val max_used : String,
            @SerializedName("is_active") val is_active : String
        )
    }
}