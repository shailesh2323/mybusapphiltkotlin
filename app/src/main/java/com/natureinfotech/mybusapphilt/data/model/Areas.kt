package com.natureinfotech.mybusapphilt.data.model

class Areas(
    var areaId : String,
    var name : String,
    var code : String
)