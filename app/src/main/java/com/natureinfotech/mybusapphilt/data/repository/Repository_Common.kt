package com.natureinfotech.mybusapphilt.data.repository

import com.google.gson.JsonObject
import com.natureinfotech.mybusapphilt.data.api.ApiHelper
import retrofit2.Response
import javax.inject.Inject

class Repository_Common @Inject constructor(private val apiHelper: ApiHelper) {
    suspend fun apiGetCity() = apiHelper.apiGetCity()

    suspend fun apiGetOffers() = apiHelper.apiGetOffers()

    suspend fun apiGetProfile() = apiHelper.apiGetProfile()

    suspend fun apiSaveProfile() = apiHelper.apiSaveProfile()

    suspend fun apiGetRoute() = apiHelper.apiGetRoute()

    suspend fun apiGetVillageByRoute(hashMap: HashMap<String, String>) = apiHelper.apiGetVillageByRoute(hashMap)

    suspend fun apiGetArea() = apiHelper.apiGetArea()

    suspend fun apiGenerateUnicode(hashMap: HashMap<String, String>) = apiHelper.apiGenerateUnicode(hashMap)
}