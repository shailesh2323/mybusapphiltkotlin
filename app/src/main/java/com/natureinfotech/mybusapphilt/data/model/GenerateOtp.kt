package com.natureinfotech.mybusapphilt.data.model

import java.io.Serializable

class GenerateOtp() : Serializable {
    var mobileNumber : String = ""
    var email : String = ""
    var isSocial : String = ""
    var name : String = ""
    var dob : String = ""
    var gender : String = ""
    var roleId : String = ""
    var smsKey : String = ""
    var otp : String = ""
}