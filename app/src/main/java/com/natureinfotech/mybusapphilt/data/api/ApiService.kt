package com.natureinfotech.mybusapphilt.data.api

import com.google.gson.JsonObject
import com.natureinfotech.mybusapphilt.data.response.*
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ApiService {
    @POST("authentication/check_version")
    suspend fun apiCheckVersion(@Body hashMap: HashMap<String, String>) : Response<CheckVersion>

    @POST("authentication/generate_otp")
    suspend fun apiGeneratedOtp(@Body hashMap: HashMap<String,String>) : Response<JsonObject>

    @POST("authentication/login_or_register")
    suspend fun apiVerifyOtp(@Body hashMap: HashMap<String, String>) : Response<VerifyOtp>

    @GET("city")
    suspend fun apiGetCity() : Response<City>

    @GET("coupon/get_offers")
    suspend fun apiGetOffers() : Response<Coupon>

    @POST("bus/bus_filter")
    suspend fun apiGetBusList(@Body hashMap: HashMap<String, String>) : Response<BusList>

    @POST("payment/generate_order_id")
    suspend fun apiGenerateOrderId(@Body hashMap: HashMap<String, String>) : Response<GenerateOrderId>

    @POST("coupon/validate_coupon")
    suspend fun apiValidateCoupon(@Body hashMap: HashMap<String, String>) : Response<JsonObject>

    @POST("bus/book_seats")
    suspend fun apiSeatBooking(@Body hashMap: HashMap<String, Any?>) : Response<JsonObject>

    @POST("user/get_profile")
    suspend fun apiGetProfile() : Response<Profile>

    @POST("user/save_profile")
    suspend fun apiSaveProfile() : Response<JsonObject>

    @GET("/route")
    suspend fun apiGetRoute() : Response<JsonObject>

    @POST("village/get_villages_by_route_id")
    suspend fun apiGetVillageByRoute(@Body hashMap: HashMap<String, String>) : Response<JsonObject>

    @GET("/area")
    suspend fun apiGetArea() : Response<JsonObject>

    @POST("user/generate_unicode")
    suspend fun apiGenerateUnicode(@Body hashMap: HashMap<String, String>) : Response<JsonObject>
}